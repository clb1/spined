## Running locally

Set up the project:

```bash
git clone https://gitlab.com/clb1/spined
cd spined/site
npm ci
```

Start the server with `npm run dev`, and navigate to [localhost:3000](http://localhost:3000).

# Thanks to Sapper and Svelte from which the framword of this documentation has been borrowed
