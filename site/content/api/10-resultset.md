---
title: Resultset
---

> Be sure to check the introduction on the [*Resultset*](docs#Resultset) concept

> In this documentation `rs` is always an Spined specific *Resultset*


### Resultset query methods

> Query methods are always returning a *Resultset*, so you can chained them

#### rs.where(`<S:idx>`, `<value>`)

Return a new *Resultset* of *Entities* that are included in the *Set Index* `<S:idx>` for the value `<value>`.

#### rs.order(`<C:idx>`)

Return a new *Resultset* of *Entities* using the order defined with the *Cursor Index* `<C:idx>`.

#### rs.filter(`<JSONata>`)

Return a new *Resultset* of *Entities* that verify the `<JSONata>`
query. See https://jsonata.org/ for all the possibilities.

For example, to filter only *Entities* with a property `primaryName`
containing the substring `Jean`, you shoudl write :

```js
rs.filter('$contains(primaryName, "Jean")')
```

> The `filter` methods has an O(N) complexity (all *Entities* of the
> called *Resultset* need to be tested). You should avoid to use it
> for very large *Resultset*. Use *Set Indexese* instead for faster
> queries.

> TODO note chaining

#### rs.sort(`<fct>`)

> TODO 

### Resultset solving methods

> Resultset solving methods always return a promise or an AsyncGenerator.

#### rs.has(`<$$id>`)

Return a promise of a boolean. True if *Entity* with id `<$$id>` is
part of the *Resultset* `rs`.

```js
const has = await rs.has('5236e2ab-71f2-4bce-9d39-2ae4d3ca0ab8')
console.log(has) // true
```

#### rs.ids()

Return a promise of a list of all *Entities* meta ids part of the
*Resultset* `rs`.

```js
const ids = await rs.ids()
console.log(ids) // ['5236e2ab-71f2-4bce-9d39-2ae4d3ca0ab8', '47bd05a9-10e5-4b5d-aa8f-3a1f955dfd07' ]
```

#### rs.count()

Return a promise of a number of *Entities* part of the *Resultset* `rs`.

```js
const count = await rs.count()
console.log(count) // 2
```

#### rs.first()

Return a promise of the first *Entity* part of the *Resultset* `rs`.

```js
const ent = await rs.first()
```

#### rs.iterate()

Return a async generator of all *Entities* part of the *Resultset* `rs`.

> Return AsyncGenerator of all results

```js
for await (const ent of all.iterate()) {
    console.log(ent.$$json)
}
```

#### rs.connection({ `<args>` })

Return a promise of a connection closely following the specifications (https://relay.dev/graphql/connections.htm).

`<args>` is an Object with either `first` or `last` as mandatory
properties with the integer value `<n>`. The connection will return an
Object with properties `edges` and `fields`properties, respectively
the `first` or `last` `<n>` "edge" in the Resultset.

An "edge" is just an Object ecapsulating an Entity and its "cursor".

If `first` is used, you can pass a cursor in the parameters `after` to
get the edges after the Entities cursor. Respectively you can pass a
cursor in `before` when you use `last`.

`desc` is a boolean (by default false) to indicates that the ordered
resulset is understood as descendant. Finally you can use `afterRank`
that take the absolute rank instead of a cursor.


```js
// in Graphql resolvers 
books: async (parent, args, ctx) => {
  const rs = parent.dimension$('book')).all()
  return connection(rs.order('C:$$createdAt'), args)
}
```

### Resultset alias methods

Some useful shortcuts alias methods are provide as syntaxic sugar:

* rs.ids(`<JSONata>`) => rs.filter(`<JSONata>`).ids()
* rs.count(`<JSONata>`) => rs.filter(`<JSONata>`).count()
* rs.first(`<JSONata>`) => rs.filter(`<JSONata>`).first()


* rs.iterate(`<C:idx>`) => rs.order(`<C:idx>`).iterate()
* rs.first(`<C:idx>`) => rs.order(`<C:idx>`).first()

