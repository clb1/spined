---
title: Reference Frame
---

> Be sure to check the introduction on the [*Reference Frame*](docs#Reference_Frame) concept


### Reference Frame definition 

There are only two ways to get a *Reference Frame* in the graph universe:

1. by calling the sp.referenceFrame() method on the graph universe handler to retrieve the implicit reference frame of a *Singularity Entity*
2. by calling the sp.defineReferenceFrame() method on the graph universe handler with one or more *Entities*


### Reference Frame properties

#### ref[alias]

You can directly get a source entity of a Reference Frame using its
corresponding alias. If no aliases where defined, you can access it
using the index of the source entities in the list parameters of
`drawReferenceFrame()`.

#### ref.default

The first source entity is always accessible via the `default` property

#### ref.isOmniscient

> * Return true if the *Reference Frame* is said to be
>   `Omniscient`. Its the case only if at least one source entities
>   has the `OMNISCIENT` trait.

You can only define a new Reference Frame using the
`sp.defineReferenceFrame()` method from entities inside an
`Omniscient` Reference Frame.

#### ref.isUnbounded

> * Return true if the *Reference Frame* is said to be
>   `Unbounded`. Its the case only if at least one source entities
>   has the `UNBOUNDED` trait.

You can only change *Entities* traits (using `ent.addTrait$()` or
`ent.addTrait$()` inside an `Unbounded` Reference Frame.


#### ref.spined

> * Return the [*Spined Graph Universe*](docs#Spined_graph_universe) handler

#### ref.aliases

> * Return the aliases name to access the source entities of the *Reference Frame*

#### ref.sources

> * Return an Object of all sources entities with aliases as keys.

