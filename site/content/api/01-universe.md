---
title: Spined graph universe
---

### Constructor Spined(params)

The constructor of a Spined universe data handler (by convention
always named `sp` in this documentation) is the imported `Spined`
module that only need one mandatory parameter: a Redis data source.

```js
import IORedis from 'ioredis'
import Spined from 'spined'

const redis = new IORedis()

const sp = Spined({ redis })
```

> params = { redis, extend?, extendFrom?, options? }
>
> * `redis` — redis client instance used to store data
> * `extend?` — dimensions model extensions
> * `extendFrom?` — read dimensions extensions from a directory
> * `options?` — TODO


### Spined handler properties


#### sp.redis

> * Return the redis connection

#### sp.extend

> * Return the current model extensions (merge of constructor parameter and loaded from file)

#### sp.options

> * Return the constructor parameters options 


### Spined handler methods

#### sp.createSingularity(`JSON`?)

The method stores a JSON data object as a new singularity [*Entity*](api#Entity).

> This is the only way for an entity to get the SINGULARITY trait

```js
const paris = await sp.createSingularity({
    name: 'Paris,
    country: 'FR',
    attractions: {
        'Eiffel Tower' => '48.8584° N, 2.2945° E',
        'Louvre' => '48.8606° N, 2.3376° E',
    },
    airports: [ 'CDG', 'ORL' ]
});

```

> * `JSON` — the JSON data object to be stored
> * Return — Promise of the new created Entity


#### sp.referenceFrame(`<$$id>`)

The method retrieve the implicit [*Reference Frame*](api#Reference_Frame) associated with 
the *Singularity Entity* having the meta id `<$$id>`. This type of *Reference Frame* have
only one source, the *Singularity Entity* itself.

> The source Entity will be accessible with alias `default` and `0`

```js
const ref = await sp.referenceFrame('715980bd-152d-43b8-b958-9e1fae3862a4');
```


> * `$$id` — an entity meta id (this entity need to have the SINGULARITY trait)
> * Return — Promise of a Reference Frame


#### sp.defineReferenceFrame([...entities] | { alias: entity1, ... })

The method can create a complex [*Reference Frame*](api#Reference_Frame)
from one or more source Entities. At least one of the source Entities
own Reference Frame sources need to have the *OMNISCIENT* trait.

The method can simply take as arguments a list of entities. Source aliases
in this case will be the index in the list (and also `default` for the first
source entity).

Another way, is to call the method with a one dimension object with aliases
as keys and entities as values. This allow a Reference Frame with custom
alias accessor.

```js
const ref = await sp.defineReferenceFrame([ e1, e2]);
// or
const ref = await sp.defineReferenceFrame({
    monument: OneTowerEntity,
    role: OneArchitectEntity,
});

```

### Spined handler helpers

#### sp.isReferenceFrame(arg)

> * Return true if the argument is a Spined ReferenceFrame

#### sp.isEntity(arg)

> * Return true if the argument is a Spined Entity

#### sp.isDimension(arg)

> * Return true if the argument is a Spined Dimension

#### sp.isIndex(arg)

> * Return true if the argument is a Spined Index

#### sp.isResultset(arg)

> * Return true if the argument is a Spined Resultset

#### sp.get$$id(arg)

> * Return the $$id if the argument is a Spined Entity or the arg itself
