---
title: Format 
---

### Redis Storage format

Low level storage of data is handled by Redis. Data is stored with a
predicatable and open sourced simple format that doesn't depends on
the current actual implementation of Spined (though they are both
optimized to work well together).

* Every Entity data and meta data is a serialized JSON object value stored at the key `<entity id>`
* Every Dimension data and meta data is a serialized JSON object value stored at the key `<dimension id>`
* A Redis hash with key `<entity id>.D` is storing all `<dimension id>` and their names of an Entity.
* A Redis set `<entity id>.M` is recording all `<dimension id>` in which an Entity is included
* A Redis set `<dimension id>.E` is recording all `<entity id>` belonging in a Dimension
* A Redis set `<dimension id>.I` is recording all indexes of a Dimension

Other specialized set with key `<dimension id>.<long hash>` are used
to optimize resultset requests but they are not necessary to restore
the data stored with Spined.
