---
title: Entity
---

> Be sure to check the introduction on the [*Entity*](docs#Entity) concept

> In this documentation `ent` is always an Spined specific *Entity*

### Entity creation

There are only three ways to create a new entity:

1. by calling the `sp.createSingularity()` method on the graph universe handler
2. by calling the `ent.duplicate$()` method describe below
3. by adding a new entity into an existing dimension with the method `dim.createEntity`


### Entity properties

#### `ent.<any>`

The JSON data object keys stored as an *Entity* automatically defines
corresponding getter properties.

```js
const ent = await john.update$({ age: 34, hobbies: ['music', 'painting'] });
console.log(john.name); // logs John Doe
console.log(john.age); // logs 34
console.log(john.hobbies); // logs [ 'music', 'painting' ]
```

> Since all meta properties, directly hanlded by Spined are prefixed
> with *$$*, you should avoid using first level properties in the JSON
> data object to be stored starting with *$$*.
> Similarly, all *Entity* methods are ended with the '$' character, so
> first level properties should also avoid this pattern.
> Note that such data would still be saved but with not direct getter
> using the `ent.<any>` syntax. You may use the syntax `ent.$$json.$$<any>`
> to access them in these cases.


#### ent.$$id

A unique *id* is assigned for each newly created *Entity*. You can
access this meta attribute using the handler `$$id`.

For this alpha relase of spined, `$$id̀` follow the uuid convention.

> [ TODO byt not yet implemented : seq / cuid / short uuid / etc ]


#### ent.$$createdAt

Any new *Entity* is created with the current timestamp stored in the `$$createdAt` meta attribute


#### ent.$$updatedAt

Anytime an *Entity* is updated, the current timestamp is retain in the `$$updatedAt` meta attribute


#### ent.$$json


*Entity* handlers in node.js are not standard JavaScript Object. To
get a pure JSON object of the data stored of any *Entity* you can call
the meta attribute `$$json`

```js
const paris = await sp.createSingularity({
    name: 'Paris,
    country: 'FR',
    airports: [ 'CDG', 'ORL' ]
});

console.log(paris.$$json)
//
// 
```

#### ent.$$json$$

Shortcut to get `$$json` object but with with `$$id̀`, `$$createdAt`,
`$$updatedAt` meta attributes value included.

#### ent.$$traits

Return the list of all *Traits* of the *Entity* `ent`

#### ent.$$dimensions

Return a _Promise_ of the list of all *Dimensions* names of the *Entity* `ent`


### Entity traits

*Entity* can be decorated by *Traits* that assert non default behaviors.


#### `SINGULARITY`

Only *Entities* created using the `sp.createSingularity()` method have this trait.

Only *Singularity Entities* can be retrieved only by knowing their
$$id to create a new *Reference Frame*.

A more numdate explanation is that *Singularity Entities* are the only
direct entry points to the graph universe.

#### `OMNISCIENT`

*Entities* created using the `sp.createSingularity()` method have this
trait by default. This trait can also be added or removed using the
`ent.addTrait$()` and `ent.delTrait$()` methods.

At least one *Omniscient Entity* need to be in the current *Reference
Frame* for a new *Reference Frame* being defined using the
`sp.drawReferenceFrame()`

#### `UNBOUNDED`

This trait can only be added or removed using the `ent.addTrait$()` and `ent.delTrait$()` methods.

At least one *Omniscient Entity* or *Unbounded Entity* need to be in
the current *Reference Frame* for `ent.addTrait$()` and
`ent.delTrait$()` methods to be available.

>*Unbounded Reference Frame* can also overwrite some meta properties
>($$id, $$created, ...) if the graph universe was created with the corresponding options
>
> Warning: NOT FULLY IMPLEMENTED YET


#### `PERPETUAL`

This trait can only be added or removed using the `ent.addTrait$()` and `ent.delTrait$()` methods.

You cannot use the `ent.delete$` method on *Pertetual Entities*.

#### `IMMUTABLE`

This trait can only be added or removed using the `ent.addTrait$()` and `ent.delTrait$()` methods.

You cannot use the `ent.update$` method on *Immutable Entities*.


#### `INVISIBLE`

This trait can only be added or removed using the `ent.addTrait$()` and `ent.delTrait$()` methods.

You cannot use the `ent.$$jon` `ent.<any>` properties on *Invisible
Entities*. Custom extension properties and methods are still
accessible.


### Entity CRUD methods

#### ent.duplicate$()

#### ent.get$()

#### ent.update$()

#### ent.inc$()

#### ent.delete$()


### Entity other methods


#### ent.be$(`ent2`)

Return true if `ent2` is strictly the same *Entity* as `ent` (return
false for two different *Entities* with different $$id even if the
stored JSON data object is identical).

> NOT yet IMPLEMENTED 

#### ent.assignDimension$(`<name>`)

Assign a new dimension the the *Entity* `ent` (Return a Promise).


#### ent.revokeDimension$(`<name>`)

Revoke a new dimension the the *Entity* `ent` (Return a Promise).

#### ent.hasDimension$(`<name>`)

A Promise of a boolean, `true` if the dimension `<name>` of the *Entity* `ent` exists.


#### ent.dimension$(`<name>`)

A Promise of the dimension `<name>` of the *Entity* `ent`.


#### ent.addTraits$(`<trait>`)




#### ent.delTraits$(`<trait>`)
