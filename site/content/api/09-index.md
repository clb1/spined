---
title: Index
---

> Be sure to check the introduction on the [*Index*](docs#Index) concept

### Index creation

There are only two ways to have an *Index* defined on a *Dimension* :

1. by calling the `dim.addIndex()` method on a *Dimension*
2. by adding an *Index* definition when assigning a new dimension with `ent.`


### Index definition

> In this documentation `idx` is always an Spined specific *Index*

#### Index types

You can define different kind of *Index* on *Dimension* and they are
classified by type. *Index* types are currently either 'U', 'S' or 'C'
which are respectively "SET", "UNIQUE" and "CURSOR" *Indexes*.

#### Index indentifier

*Index* identifier are formed by the concatenation of their type (`U`,
`S`, or `C`) the separator `:`, an optional *Index* argument followed
by `:` and a JSONata query.

#### Index JSONata query

*Indexes* are created on a specific JSONata query that map every
*Entity* JSON document to a value from the JSONata query
evaluation. In most cases, that would be just the value of a specific
property inside the JSON document but any complex of JSONata query is
possible.

You can also use the meta properties `$$createdAt` and `$$updateAt` in
the JSONata query.

> complex JSONata query may slightly impact performance when you add a
> very large number of new *Entities* in batch to an indexed
> *Dimension*.

### Index type `S` (SET)

Type `S` *Indexes*, are just a subset of the *Dimension* that verify a
specific JSONata query (i.e. evaluate to truthy value).

This type of index is well suited to query really fast a *Dimension*
subset that verify a particular criteria. For example, you could set a
*Index* on the "city" property and be able to search or count
*Entities* efficiently on any specific city subset.

#### idx.all(`<value>`)

Return a new *Resultset* of *Entities* that verify `<value>` for the
*Index* `idx`.

#### `S` alias methods

* idx.ids(`<str>`) => idx.all(`<str>`).ids
* idx.count(`<str>`) => idx.all(`<str>`).count
* idx.first(`<str>`) => idx.all(`<str>`).first
* idx.iterate(`<str>`) => idx.all(`<str>`).iterate

### Index type `U` (UNIQUE)

You can declare that all *Entities* in a *Dimension* verify a unicity
condition for a specific JSONata query result (typically used to
guarantee that a property value is unique for each *Entities* of the
*Dimension* like `email` in a "Account" *Dimension*, but any query can
be used).

#### idx.find(`<value>`)

Return a Promise of the *Entity* for which is indexed unique value is
`<value>`. Returns null if no entity is found.


#### idx.findElseThrow(`<value>`)

Same as find but throw if no *Entity* is found.


### Index type `C` (CURSOR)

*Cursor Indexes* are used create an ordered set of all *Entities* in a
*Dimension*. The JSONata query is used to calculate the key to sort
all their *Entities*. The sort order can be alphanumeric or numeric
depending the identifier argument, respectively `AlphaNum` or `Num`.

> If you don't mention the sort order mapping, it default to
> `AlphaNum` except on the meta attributes $$createdAt and $$updatedAt
> for which it is `Num`

*Cursor Indexes* are primaryly used as argument to the rs.order() method.


#### idx.rank(id)

Return a promise of the rank the Entity with id `id` in the ordered by set.

