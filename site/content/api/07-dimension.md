---
title: Dimension
---

> Be sure to check the introduction on the [*Dimension*](docs#Dimension) concept

> In this documentation `dim` is always an Spined specific *Dimension*

### Dimension creation

There is only one way to add a new dimension to a specific *Entity* :

1. by calling the `ent.assignDimension$(<name>)` method on an *Entity*


### Dimension properties

#### dim.name

Return the name of the dimension `dim`.

Dimension names are unique for each *Entity*


#### dim.schema

> TODO

#### dim.indexes

> TODO

#### dim.extend

> TODO

#### dim.$$id

Each *Dimension* has also a unique meta *id*. You can access it using the handler `id`.

### Dimension mutation methods

#### dim.createEntity(`<JSON>`)

The method stores a JSON data object as a new [*Entity*](api#Entity) and
directly add this new *Entity* to the *Dimension* `dim`.

#### dim.addEntity(`ent`)

Add the existing *Entity* `ent` to the *Dimension* `dim`.

#### dim.removeEntity(`ent`)

Remove the existing *Entity* `ent` to the *Dimension* `dim`.

> WARNING, GARBAGE collection is not yet implemented...

#### dim.addIndex()
#### dim.removeIndex()
#### dim.setSchema()

### Dimension query methods

#### dim.has(`ent`|`<$$id>`)

Return true if the *Entity* `ent` (or the meta id `<$$id>`) is part of the dimension `dim`.

#### dim.retrieve(`<$$id>`)

Return the *Entity* with the meta id `<$$id>` which is part of the dimension `dim`.

#### dim.all()

Return a new *Resultset* with all *Entities* of the dimension `dim`.


### Dimension alias methods

Some useful shortcuts alias methods are provide as syntaxic sugar:

* dim.filter => dim.all().filter
* dim.order => dim.all().order
* dim.where => dim.all().where

* dim.iterate => dim.all().iterate
* dim.first => dim.all().first
* dim.ids => dim.all().ids

These last two shorcuts provide the same results as their long version
but are slightly more optimized when called directly from the dim
handler :

* dim.count => dim.all().count
* dim.has => dim.all().has

