---
title: Dimension extensions
---

A dimension name can be statically extended with schema constraints,
triggers and behavioral methods.

Any concrete dimension of an *Entity* having that identical name would 
be bound with these extensions.

> TODO extensions are not yet fully documented

<!-- extensions or models ? -->

### Loading extensions

The current most convenient way to add extensions is to use the `extendFrom` option
of the `Spined()` Constructor.

The option take a directory as argument. Every `<dimension name>.js`
files in this directory with a default Object export will inject all
extensions under the properties `schema`, `triggers`, `getters`,
`methods`, `options` for all *Dimensions* whith that name.

Very simple extension file :

```js
const extensions = {
  schema: {
    title: 'Account',
    type: 'object',
    required: ['email'],
    properties: {
      email: { type: 'string' }
    }
  },
  triggers: {
    beforeDelete: ($ref, attributes) => !attributes.isActive
  },
  getters: {
    fullName: ($ref, self) => () => self.firstName + ' ' + self.lastName,
  }
  methods: {
    toString: ($ref, self) => () => {
      return `${self.slug}`
   }
  }
}
export default extensions
```


### schema extension

You can bind a JSONschema to a dimension name. Entities JSON data will
need to verify the JSONschema to be able to added to the dimension.

### triggers extensions

#### beforeCreate: (`$ref`, `attributes`) => `attributes`
#### beforeUpdate: (`$ref`, `attributes`) => `attributes`
#### beforeDelete: (`$ref`, `attributes`) => `boolean`

### getters extensions

#### `getterName`: (`$ref`, `self`) => () => Any

### methods extensions

#### `methodName`: (`$ref`, `self`) => (Any args) => Any

### options extensions


#### inMemoryTtl
