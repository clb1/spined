---
question: Spined is not 1.0 yet. Is it production ready and should I use it?
---

Although "production readiness" is very subjective notion, you should
be ready to many breaking API changes and bugs at this stage.  So it
is not encouraged to use in production environement. But we are happy
if you try it, experiment with it and contribute.
