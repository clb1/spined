---
title: Quick start
---

In this section, you will learn how to use Spined in very simple cases: 
* basic operations to create and update persistent data
* introduction to the Spined relationship paradigm (semantic network)
* queries operations principles

### Using Spined in Node.js

Before being able to use a Spined universe data handler, you first
need to import the `IORedis` and `Spined` modules and create a Redis
connection (check Redis documentation for the many options of a new
Redis connection). For example, using the default Redis port on a
local Redis installation :

```js
import IORedis from 'ioredis'
import Spined from 'spined'

const redis = new IORedis()
```

> `IORedis` comes as a peer dependency, so you may have to install it
> in your own project with `npm install ioredis`


You can now create a Spined universe data handler (by convention
always named `sp` in this documentation) using the imported `Spined`
module. You need to pass an object options argument containing at
least the `redis` connection previously created:

```js
const sp = Spined({ redis })
```

> Many Spined operations are asynchronous and return a Promise. We
> will use await/async notation in this documentation but you may also
> use callback with `.then`


### Adding persistent data in the store

To add our first persistent data in the store, we will use the method
`createSingularity`. It takes a JSON compatible JavaScript Object as
argument:

```js
const john = await sp.createSingularity({ name: 'John Doe', age: '33', hobbies: ['music', 'cinema'] })
```

> JSON is a subset of JavaScript Object. A JSON compatible Javascript
> Object should not contain data types which are not supported by JSON
> (undefined, BigInt, Symbol or function). To learn more about the
> differences, read [What is the difference between JSON and Object
> Literal
> Notation?](https://stackoverflow.com/questions/2904131/what-is-the-difference-between-json-and-object-literal-notation)

A corresponding serialized JSON document is stored in Spined as an
abstract representation called an *Entity*. An *Entity* contains the
JSON data Object argument above, a unique *id* (usually following uuid
format) and several meta information like the timestamps of the
creation and the last update of the stored data.

> A *Singularity* is an special *Entity* that has been created without
> any relationship with any other *Entities*.

### Using the node.js *Entity* handler

The nodejs constant `john` in the previous § is a Spined *Entity*
handler that can be used to perform operation on the stored data.

You can for example fetch the value of the property `age`:

```js
console.log(john.age) // logs 33
```

You can also fetch the meta id property named `$$id`:

```js
console.log(john.$$id) // logs ad1b8c32-17cf-4f1e-9265-78712295289b
```

And the meta timestamp corresponding to the creation of the `john`
entity:

```js
console.log(john.$$createdAt) // logs 2020-10-26T23:15:57.130Z
```

> Fetching properties and meta information on an *Entity* handler is a
> synchronous operation because Spined maintain a cache data in memory
> when its handler in Node.js exists.

> Meta properties are always prefixed with $$


### Updating entity data


The method `update$`on the *Entity* handler will update or create all
properties existing in its argument JSON object:


```js
await john.update$({ age: 34, hobbies: ['music', 'painting'] })
console.log(john.name) // logs John Doe
console.log(john.age) // logs 34
console.log(john.hobbies) // logs [ 'music', 'painting' ]
```

> Notice that to avoid collision with property names, all *Entity*
> methods like `update$` are postfixed with the symbol `$`

### Delete an entity from the store


The method `delete$`on the *Entity* handler will remove it permanently
from the store:

```js
await john.delete$()
console.log(john.name) // 
```

### Relationships

You can define for any *Entity* one or several named *Dimensions*.

Each *Dimension* is defining semantically designated set of relationships with other *Entities*.

Let's define a new *Singularity Entity* `library` with its two *Dimensions* `Ẁriter` and `Musician`:

```js
const library = await sp.createSingularity({ address: '221B Baker Street, London, England' })
const WritersCollection = await libray.assignDimension$('WriterCollection')
const MusiciansCollection = await libray.assignDimension$('MusicianCollection')
```

> To better distinguish *Entities* and *Dimensions* in your code, we
> encourage you to use capitalized names for *Dimension* name and their
> respective Node.js handlers constants.

Addind a new *Entity* into the *Dimension* `MusicianCollection` could be
done using the method `addEntity`:

```js
const lenon = await sp.createSingularity({ name: 'John Lenon', language: 'English' })
await MusicianCollection.addEntity(lenon)
```

But most of the time, you won't be creating *Singularity Entities* to
add them afterwards into a dimension set. Instead, you can create and
add a new entity directly into a *Dimension* in one step using the
dimension method `createEntity`:

```js
const shakespeare = await WritersCollection.createEntity({ name: 'William Shakespeare', language: 'English' })
```

There are no limits in the number of *Dimensions* an *Entity* can be
member of. Let's create a new entity member of the `Musician`
*Dimension* and add it to the `WriterCollection` dimension set:

```js
const leonard = await MusicianCollection.createEntity({ name: 'Leonard Cohen, language: 'English' })
await WriterCollection.addEntity(leonard);
```

> Entities created using createEntity can only be accessed through the
> *Dimensions* they are part of. This is the a base Spined principle
> to restrict and control access to a subset of data.


### Simple queries

Spined being a JSON focused data store, we are using JSONata, a rich
existing query language to search entities inside a dimension. But
let's add a few more writers documents in our dimension set `WriterCollection`
before writing our first queries:

```js
await WriterCollection.createEntity({ name: 'Molière', language: 'French' });
await WriterCollection.createEntity({ name: 'Racine', language: 'French',  });
await WriterCollection.createEntity({ name: 'William Butler Yeats' language: 'English', nobel: true });
await WriterCollection.createEntity({ name: 'Henri Bergson', language: 'French', nobel: true });
```

To search all entites of the Dimension `WriterCollection` having `English` has
value of the property `language`, you can use the dimension method
`filter` as follow and `iterate` on the *Resultset*:

```js
const writers = WriterCollection.filter('language = "English"');
for await (const a of writers.iterate()) {
  console.log(a.name);
}
```

All writers with a name containing the string `ea` :

```js
const writers = await WriterCollection.filter('$contains(name, "ea")');
for await (const a of writers.iterate()) {
  console.log(a.name);
}
```

All writers with a nobel price : 

```js
const writers = await WriterCollection.filter('nobel');
for await (const a of writers.iterate()) {
  console.log(a.name);
}
```

> Queries are only limited by the the expressesiness of JSONata

> TODO intro resultset and chaining...
