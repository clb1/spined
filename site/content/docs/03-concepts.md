---
title: Concepts
---

### Graph Universe

Forget about flat and restricted bi-dimensional "table<=>record"
models for your data, Spined allows multi-dimensional complex data
architectures as a rich graph.

The Spined graph universe is easy to understand and allow both
extremely simple model of data and conceptually rich sub-graphs of
information securely isolated from each other.

The first thing to know is that the Spined graph universe is
just a big collection of the same thing: *Entities*.

### Entity

In Spined, an *Entity* can represent anything from a specific entry
point to the graph universe to any abstract or concrete data instance
you want to store (*Entity* could be products, customers, but also
roles, permissions, etc). They are the nodes (or vertices) of the
graph universe.

Simply put, an *Entity* is a set of data that you want to keep
persistent record of as a block. There are very few restrictions on
the size or the internal structuration of this data set. It just needs
to be stored as JSON data object and be smaller that what the current
Redis backend storage allows (512 Megabytes).

Each *Entity* being a JSON document decorated with a few meta
information, it can have any shape allowed by the JSON standard
format. For example, an "invoice" *Entity* would be able to store its
lines in an array property and a person would be able to store many
types of phone numbers and address without being contrainst by the
very limited concept of "columns".

So JSON as the base data format liberates your data from the jail of
the RDBMS "columns" concept but how do you manage relationships
between *Entities* in the graph universe? There are handled in Spined
using the concept of "*Dimension*".

### Dimension

In Spined, a *Dimension* is not a relationship in itself but a
category (you could also say a "class" or a semantic link definition)
of relationships between an *Entity* and other *Entities*.

Let's say we have an *Entity* representing a company "ACME corp.", we
could define a new *Dimension* "Employee" for this "ACME" *Entity* to
store this specific semantic relationship "to be an employee" with
other *Entities* representing persons.

> TODO drawing

By default, new *Entities* in the graph Universe have no *Dimensions*,
but you can add as many as you want. In our example, we could add the
*Dimensions* "Products", "Suppliers", etc, to the "ACME" *Entity*.

The Spined graph universe is oriented, thus a relationship from the
*Entity* "A" to the *Entity* "B" defined by the *Dimension* "X"
doesn't imply any symetric Relationship Between *Entity* "B" towards
*Entity* "A".

Each *Dimension* has a name and every *Dimensions* in the graph
universe sharing the same name may be statically bound to common
behaviors and schema conditions for their constituent *Entities*. For
example, "being an employee" is not a kind of relationship only valid
for the company "ACME corp." but for any company. Thus you could add
the same *Dimension* named "Employee" for many other *Entities*
representing companies and add a condition that any person *Entity*
added to any of these "Employee" Dimensions being older than 16 years
old before allowing a person in the *Dimension* "Employee".

In the graph theory, Spined *Dimensions* are just labels for oriented
links between nodes. The decorated graph is forming a semantic
network.

#### normalisation

When building a spined model, you often can choose two different
directions to represent a set of data that can be linked to an Entity:

* As a subdata set included in the JSON data document of the *Entity* itself
* As a set of separate *Entities* whose relationship with the main *Entity* is recorded as a specific "dimension"

One example would be a person *Entity* owning some books. You could
store the books information as an array inside the JSON document of
the person *Entity* *or* create a *Dimension* "OwnedBook" for that person
and put into it as many book *Entities* as necessary.

The choice will often depends on the type of application you are building but 
some general rules can be drawn:

* for very small subset of data, it is often more practical and
  efficient to store them inside the JSON document of the main entity
  even at the cost of small repetitions in the backend storage.
  
* for structured subset of data with more than one *Entity* relationship,
  it preferable to store them in separate entities, specially if their
  relationships cardinality vary.
  
* finally, for subset of data that are updated or deleted
  independently, it is nealy always a better idea to store them as
  separate *Entities*.
  
  
So to come back with the Person/OwnedBooks example above, you would 
prefer store books as separate *Entities* if one book can be owned by 
more than one person. If a book information dataset needs to be updated
(selling statistics or price history), you absolutely needs to store them
as separate *Entities* to keep your life simple.


#### schema & model 

You can modelize a *Dimension* in greater detail than just a name, and
specify that all *Entities* in a specific *Dimension* *must* obey a particular
JSONschema description.

You can also define model methods and trigger to get callable for all
entities in a specific *Dimension* (for example a method sendInvoice callable
on any *Entity* in the *Dimension* "invoice" of a client).

Schema, model methods and triggers are statically *bound* to a
particular *Dimension* name, meaning that any *Dimension* with that name
will inherit these (for example the "sendInvoice" method bound to all
*Dimensions* "Invoices", for any suppliers or clients).


### Resultset

A *Resultset* represents a set of *Entities* selected via differents
type of query methods. *Resultsets* can be chained, and you can also
create a new *Resultset* via union, intersection and exception set
operations.

> union, intersection and exception are not yet implemented

### Index

You may define *Indexes* on *Dimensions* to allow faster search and
ordering of *Entities*. Spined currently support 3 types of *Index* :

* *Set Index* are grouping *Entities* inside a *Dimension* depending
  on the value of a simple property of each *Entity* JSON document or
  even a complex result value from a JSONata query. This type of
  *Index* allow faster search inside a big *Dimension* filtering by
  the indexed value.

* *Unique Index* enforce a unicity condition on *Entities* belonging
  to a *Dimension* on a simple property or JSONata query result. It
  allows O(1) retrieval of the *Entity* mapped to that value. This
  *Index* is ideal to retrieve instantly an *Entity* with a unique
  identifier, as a classic example an "email" for an *Dimension*
  "Account".

* *Cursor Index* are organizing all *Entities* of a *Dimension* inside
  a sorted set (again either on the value a simple property or a full
  JSONata query result). This is ideal to implement cursor-based or
  offset-based pagination on big dataset.


### Reference Frame

All operations on *Entities* and *Dimensions* are operating in the
context of a *Reference Frame*. A reference frame is simply a non
empty list of other special *Entities* of the graph universe (named
*Reference Sources*) that give a "point of view" on these all
operations. The semantic to associate with a *Reference Frame* is not
pre-determined and is enterily of your choosing.

One simple illustration of this concept would be to assign different
behaviors on the method "engineOn" of an "Entity" representing a
car. The method could test if the reference frame include an *Entity*
refrence source "Account" and only allows the "engineOn" method if it
matches the owner property of the car in the JSON data object.

A *Reference Frame* is always implicitly inherited automatically in
all operations results (*Entities*, *Dimensions*, *Indexes* and
*Resultset*). You usually start your software workflow with a very
generic *Reference Frame* from a *Singularity* (see below) and use
smaller references frames to manage different contexts (typical
examples would be role permissions or specialized behaviors).


#### Singularity

A *Singularity* is a special *Entity* that has been created
"ex-nihilo" with no prior relationship with any other *Entities*. They
are starting points in the graph universe and the only *Entities*
searchable by their meta id without knowing anything else about them.

Looking up for a *Singularity Entity* automatically defines a default
Reference Frame including only the singularity itself as reference
sources.

A simple query is usually a search in a *Dimension* of a single
*Entity*.  In this case we the *Reference Frame* is the *Entity* on
which you apply the search.

But a *Reference Frame* can contain more than one *Entities* to allow
advanced search... 

> [ TODO complete with example ]


