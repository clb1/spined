---
title: Introduction
---

### Before we begin

> Spined is in very early development phase, and many things,
> including the API, are going to change often and break
> things. Please consider it as experimental software that must not be
> used in production but if you do it, it's entirely at your own
> risks. Also, this document is a work-in-progress and incomplete,
> sometimes lagging with new features implementation. But if you get
> stuck, have questions, need help, or want to contribute reach out
> in the [Discord chatroom](https://discord.gg/npCMj8).

### What is Spined?

Spined is a data store (or database): a software to create and
manipulate efficiently persistent data. Spined organize data as a
[semantic network](https://en.wikipedia.org/wiki/Semantic_network), a
flexible and high level paradigm.

Spined is extremely opinionated and focus on storing
[JSON](https://www.json.org/json-en.html) type documents, using
[JSONSchema](https://json-schema.org/) to enforce structure and
[JSONata](https://jsonata.org/) as indexes and queries descriptor.

Spined is written in [Node.js](https://nodejs.org/en/) and bindings
are currently only available for Node.js.

The low level storage implementation uses [Redis](https://redis.io/)
and Spined generally preserve the high-performance of Redis and in
some cases provide even faster performance by using an "in memory"
application cache.

As an abstract front-end to Redis, it is part of the NoSQL database
family but try bring back high level organisation of your data as in
more traditional relational database. But far from a table oriented
abstraction, Spined provide a graph-type model for easier and more
human-understandable queries and mutation.

Spined never hide the lower level implementation and direct access to
the Redis API access is always an option.

We have put a particular emphasis on implicit low level data
separation security and enabling easy modern REST an
[GraphQL](https://graphql.org/) backend queries.


### Thanks

Many thanks to all open source softwares projects linked above,
without them Spined would not exist.

Hat tip for the [Svelte](https://svelte.dev/) and
[Sapper](https://sapper.svelte.dev/) teams. They have a great
framework and this documentation engine is forked from Sapper own
documentation.
