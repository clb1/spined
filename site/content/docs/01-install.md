---
title: Install
---

### Prerequisites

* Node.js version 14 or later version 
* An access to a Redis store

### Getting started

To use Spined, you need to install the npm [spined](https://gitlab.com/clb1/spined) package :

```bash
npm install spined
# or: yarn add spined
