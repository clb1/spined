"use strict";

var _ioredis = _interopRequireDefault(require("ioredis"));

var _spined = _interopRequireDefault(require("./spined.js"));

var _dataSamples = require("../test/data-samples");

var _scoreMaps = _interopRequireDefault(require("./score-maps"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
const redis = new _ioredis.default({
  port: process.env.REDIS_PORT,
  db: 2
});
const sp = (0, _spined.default)({
  redis
});

const run = async () => {
  let dims = await (0, _dataSamples.mini)(sp);
  console.log('end');
  console.log('AlphaNumMap', _scoreMaps.default);
};

run();
//# sourceMappingURL=local.js.map