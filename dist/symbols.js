"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UNIVERSE = exports.TRAITS = exports.S = exports.RESULTSET = exports.REFERENCE = exports.OBJECT = exports.NORMALIZE = exports.INDEXTYPE = exports.INDEX = exports.EO = exports.ENTITY = exports.DO = exports.DIMENSION = void 0;
const S = Symbol('Spined'); // TODO replace with UNIVERSE

exports.S = S;
const UNIVERSE = Symbol('Spined.Universe');
exports.UNIVERSE = UNIVERSE;
const REFERENCE = Symbol('Spined.Reference'); // internal stored Entity & Dimension object

exports.REFERENCE = REFERENCE;
const EO = Symbol('Spined.EO');
exports.EO = EO;
const DO = Symbol('Spined.DO');
exports.DO = DO;
const ENTITY = Symbol('Spined.Entity');
exports.ENTITY = ENTITY;
const TRAITS = Symbol('Spined.Traits');
exports.TRAITS = TRAITS;
const OBJECT = Symbol('Spined.Object');
exports.OBJECT = OBJECT;
const DIMENSION = Symbol('Spined.Dimension');
exports.DIMENSION = DIMENSION;
const RESULTSET = Symbol('Spined.Resultset');
exports.RESULTSET = RESULTSET;
const INDEX = Symbol('Spined.Index');
exports.INDEX = INDEX;
const INDEXTYPE = Symbol('Spined.IndexType');
exports.INDEXTYPE = INDEXTYPE;
const NORMALIZE = Symbol('Spined.MN');
exports.NORMALIZE = NORMALIZE;
//# sourceMappingURL=symbols.js.map