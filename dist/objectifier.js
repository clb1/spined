"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.unserializeEntity = exports.unserializeDimension = exports.serializeEntity = exports.serializeDimension = exports.default = void 0;

var _symbols = require("./symbols");

var _traits = _interopRequireDefault(require("./traits"));

var _meta = _interopRequireDefault(require("./meta"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const NEXT = Symbol('Spined.mqueue');
const UNTIL = Symbol('Spined.until'); // TODO configuration to change default functions

const serialize = o => JSON.stringify(o);

const unserialize = async (id, redis) => {
  // TODO non compressed case depending on options
  const s = await redis.get(id);
  if (!s) throw new Error(`spined: ${id} doesn't exist in Redis`);
  return JSON.parse(s);
}; // weak ref for isDirty ?


const binaryEncode = (list = [], table) => list.reduce((acc, k) => acc + table[k], 0);

const binaryDecode = (b, table) => Object.keys(table).filter(k => table[k] & b);

const serializeEntity = (o, redis) => {
  delete Object.assign(o, {
    u: o.id
  }).id;
  delete Object.assign(o, {
    t: binaryEncode(o.traits, _traits.default)
  }).traits;
  delete Object.assign(o, {
    a: o.attributes
  }).attributes;
  delete Object.assign(o, {
    c: o.createdAt
  }).createdAt;
  delete Object.assign(o, {
    m: o.updatedAt
  }).updatedAt;
  return serialize(o, redis);
};

exports.serializeEntity = serializeEntity;

const unserializeEntity = async (id, redis) => {
  const o = await unserialize(id, redis);
  delete Object.assign(o, {
    id: o.u
  }).u;
  delete Object.assign(o, {
    traits: binaryDecode(o.t, _traits.default)
  }).t;
  delete Object.assign(o, {
    attributes: o.a
  }).a;
  delete Object.assign(o, {
    createdAt: o.c
  }).c;
  delete Object.assign(o, {
    updatedAt: o.m
  }).m;
  Reflect.defineProperty(o, _symbols.EO, {
    enumerable: false,

    get() {
      return true;
    }

  });
  return o;
};

exports.unserializeEntity = unserializeEntity;

const serializeDimension = (o, redis) => {
  delete Object.assign(o, {
    u: o.id
  }).id;
  delete Object.assign(o, {
    e: o.entityId
  }).entityId;
  delete Object.assign(o, {
    n: o.name
  }).name;
  delete Object.assign(o, {
    x: binaryEncode(o.meta, _meta.default)
  }).meta;
  delete Object.assign(o, {
    s: o.schema
  }).schema;
  delete Object.assign(o, {
    c: o.createdAt
  }).createdAt;
  delete Object.assign(o, {
    m: o.updatedAt
  }).updatedAt;
  return serialize(o, redis);
};

exports.serializeDimension = serializeDimension;

const unserializeDimension = async (id, redis) => {
  const o = await unserialize(id, redis);
  if (!o.n) throw new Error(`spined: ${o.u} is not a valid dimension id`);
  delete Object.assign(o, {
    id: o.u
  }).u;
  delete Object.assign(o, {
    entityId: o.e
  }).e;
  delete Object.assign(o, {
    name: o.n
  }).n;
  delete Object.assign(o, {
    meta: binaryDecode(o.x, _meta.default)
  }).x;
  delete Object.assign(o, {
    schema: o.s
  }).s;
  delete Object.assign(o, {
    createdAt: o.c
  }).c;
  delete Object.assign(o, {
    updatedAt: o.m
  }).m;
  Reflect.defineProperty(o, _symbols.DO, {
    enumerable: false,

    get() {
      return true;
    }

  });
  return o;
};

exports.unserializeDimension = unserializeDimension;
const inMemory = {};
let count = 0;
let first = null;
let last = null; // defaultInMemoryTtl in ms - default 1 hour
// inMemoryMax : number of object in memory

const Objectifier = ({
  redis,
  defaultInMemoryTtl = 3600000,
  inMemoryMax = 20000
}) => {
  if (defaultInMemoryTtl > 3600000 * 24) throw new Error('spined: too big defaultInMemoryTtl');

  const saveInMemory = (o, ttl) => {
    // console.log('saveInMemory', Object.keys(inMemory), count, JSON.stringify(o));
    if (Object.prototype.hasOwnProperty.call(inMemory, last)) {
      inMemory[last][NEXT] = o.id;
    }

    inMemory[o.id] = o;
    inMemory[o.id][UNTIL] = new Date().getTime() + ttl;
    count = count + 1; // XXX is it faster than length ?

    last = o.id;

    if (!first) {
      first = o.id;
    } else if (count > inMemoryMax) {
      const second = inMemory[first][NEXT]; // inMemory[first][DIRTY] = true // will be manganed upstream

      delete inMemory[first];
      count = count - 1;
      first = second;
    }

    return o;
  };

  const store = serializer => async ({
    id,
    createdAt,
    updatedAt,
    ...others
  }, {
    inMemoryTtl = defaultInMemoryTtl
  } = {}) => {
    // TODO non compressed case depending on options
    if (!id) throw new Error('spined: invalid object without id');
    const now = new Date().getTime();
    const o = {
      id,
      ...others,
      createdAt: createdAt || now,
      updatedAt: updatedAt || now
    };
    Reflect.defineProperty(o, _symbols.EO, {
      enumerable: true,

      get() {
        return true;
      }

    }); // TODO if another client has updated before us, throw()

    await redis.set(id, serializer({ ...o
    }));
    if (inMemoryTtl === 0) return o;

    if (Object.prototype.hasOwnProperty.call(inMemory, id) && now < inMemory[id][UNTIL]) {
      Object.keys(o).forEach(k => {
        inMemory[id][k] = o[k];
      });
      return inMemory[id];
    }

    return saveInMemory(o, inMemoryTtl);
  };

  const retrieve = unserializer => async (id, {
    inMemoryTtl = defaultInMemoryTtl
  } = {}) => {
    // TODO put id back on TOP. need PREV not sure it worth it
    if (Object.prototype.hasOwnProperty.call(inMemory, id) && new Date().getTime() < inMemory[id][UNTIL]) return inMemory[id];
    const o = await unserializer(id, redis);
    return saveInMemory(o, inMemoryTtl);
  };

  const deleteEntity = async id => {
    inMemory[id][UNTIL] = 0; // invalidate

    await redis.del(id);
    inMemory[id].deletedAt = new Date().getTime();
    delete inMemory[id].attributes;
    return true;
  };

  return Object.freeze({
    storeEntity: store(serializeEntity),
    storeDimension: store(serializeDimension),
    retrieveEntity: retrieve(unserializeEntity),
    retrieveDimension: retrieve(unserializeDimension),
    deleteEntity,
    // TODO better stats
    status: () => ({
      length: Object.keys(inMemory).length,
      count,
      first,
      last
    })
  });
};

var _default = Objectifier;
exports.default = _default;
//# sourceMappingURL=objectifier.js.map