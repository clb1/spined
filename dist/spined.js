"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _objectifier = _interopRequireDefault(require("./objectifier"));

var _helpers = require("./helpers");

var _symbols = require("./symbols");

var _refframe = _interopRequireDefault(require("./refframe"));

var _entity = _interopRequireDefault(require("./entity"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const Spined = ({
  redis,
  extend = {},
  extendFrom,
  options = {}
}) => {
  const objectifier = (0, _objectifier.default)({
    redis
  });
  const schemaKeywords = new Set([]);
  const $spined = {
    get redis() {
      return redis;
    },

    get extend() {
      return extend;
    },

    get options() {
      return options;
    },

    get schemaKeywords() {
      return schemaKeywords;
    },

    isSpined: x => x && !!x[_symbols.S],
    isEntity: x => x && !!x[_symbols.ENTITY],
    isDimension: x => x && !!x[_symbols.DIMENSION],
    isResultset: x => x && !!x[_symbols.RESULTSET],
    isIndex: x => x && !!x[_symbols.INDEX],
    isReferenceFrame: x => x && !!x[_symbols.REFERENCE],
    get$$id: x => x[_symbols.ENTITY] ? x.$$id : x,
    addSchemaKeyword: (...args) => schemaKeywords.add(args) // XXX TODO hash ? or Array ?

  };
  Reflect.defineProperty($spined, _symbols.S, {
    enumerable: false,
    value: true
  });
  Reflect.defineProperty($spined, _symbols.UNIVERSE, {
    enumerable: true,
    value: true
  });
  Reflect.defineProperty($spined, 'redis', {
    enumerable: false
  });
  Reflect.defineProperty($spined, 'options', {
    enumerable: true
  });

  $spined.dump = (any, {
    depth = 1
  } = {}) => {
    if (!any || !any[_symbols.S]) {
      throw new Error('dump argument should be an Entity, Dimension, Index');
    }

    console.dir(any, {
      getters: true,
      depth
    });
  }; // should be move in plugin id


  $spined.id = any => {
    if (any == null) throw new Error('Spined.id attempt on null value');
    if (any[_symbols.ENTITY]) return any.$$id;
    if ((0, _helpers.isId)(any)) return any;
    throw new Error(`${any} is neither Entity or id`);
  };

  const loadExtendFrom = async () => {
    const files = _fs.default.readdirSync(extendFrom);

    await Promise.all(files.map(async f => {
      if (/^[a-zA-Z]+\.js$/.test(f) && _fs.default.lstatSync(`${extendFrom}${f}`).isFile()) {
        const module = await Promise.resolve(`${extendFrom}${f}`).then(s => _interopRequireWildcard(require(s))); // Better filtering (use symbol ?)

        if (module.default && module.default.schema) {
          const dimName = f.split('.')[0]; // console.log(`Loading '${dimName}' dimension extend from file ${extendFrom}${f}`);

          extend[dimName] = module.default;
        }
      }
    }));
    extendFrom = null;
  };

  $spined.createSingularity = async (attributes = {}) => {
    if (extendFrom) await loadExtendFrom();
    const o = await objectifier.storeEntity({
      id: (0, _helpers.makeId)(),
      attributes,
      traits: ['SINGULARITY', 'OMNISCIENT', 'UNBOUNDED']
    });
    redis.sadd(`[spined].singularity`, o.id);
    return (0, _entity.default)({
      objectifier,
      $ref: (0, _refframe.default)($spined, objectifier, o)
    }, o);
  };

  $spined.referenceFrame = async id => {
    if (extendFrom) await loadExtendFrom();
    const o = await objectifier.retrieveEntity(id);
    if (!o.traits || !o.traits.includes('SINGULARITY')) throw new Error('[spined] not a singularity, cannot retreive reference frame');
    return (0, _refframe.default)($spined, objectifier, o);
  };

  $spined.defineReferenceFrame = async (...entities) => {
    let test = false;
    let aliases = [];

    if (typeof entities[0] === 'object' && entities[0] !== null && !entities[0][_symbols.ENTITY]) {
      aliases = Object.keys(entities[0]);
      entities = Object.values(entities[0]);
    }

    const objects = [];

    for (const e of entities) {
      if (!e[_symbols.ENTITY]) throw new Error('[spined] only entities can define reference frame');
      test = test || e.$$ref.isOmniscient;
      const o = await objectifier.retrieveEntity(e.$$id);
      objects.push(o);
    }

    if (!test) throw new Error('[spined] need omniscient reference frame to define a new one');
    return (0, _refframe.default)($spined, objectifier, objects, aliases);
  };

  return Object.freeze($spined);
};

var _default = Spined;
exports.default = _default;
//# sourceMappingURL=spined.js.map