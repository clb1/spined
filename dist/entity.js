"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _helpers = require("./helpers");

var _handler = require("./handler");

var _symbols = require("./symbols");

var _dimension = _interopRequireDefault(require("./dimension"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// ctx is not directly accessible
const Entity = (ctx, o) => {
  const getIndex = async ({
    dimName,
    constraint
  }) => {
    const dimension = await (0, _helpers.getDimension)(ctx, o)(dimName);
    return dimension.index(constraint);
  }; // XXX only use defineProperty ?


  const $entity = {
    get $$json() {
      return JSON.parse(JSON.stringify(o.attributes));
    },

    get $$createdAt() {
      return new Date(o.createdAt).toJSON();
    },

    get $$updatedAt() {
      return new Date(o.updatedAt).toJSON();
    },

    get $$json$$() {
      return JSON.parse(JSON.stringify({
        $$id: o.id,
        $$createdAt: new Date(o.createdAt).toJSON(),
        $$updatedAt: new Date(o.updatedAt).toJSON(),
        ...o.attributes
      }));
    },

    get $$traits() {
      return o.traits || [];
    }

  };
  Reflect.defineProperty($entity, _symbols.S, {
    value: true
  });
  Reflect.defineProperty($entity, _symbols.ENTITY, {
    enumerable: false,
    value: true
  });
  Reflect.defineProperty($entity, '$$id', {
    enumerable: true,

    get() {
      return o.id;
    }

  });
  Reflect.defineProperty($entity, '$$json', {
    enumerable: false
  });
  Reflect.defineProperty($entity, '$$json$$', {
    enumerable: false
  });
  Reflect.defineProperty($entity, '$$traits', {
    enumerable: false
  });
  Reflect.defineProperty($entity, '$$ref', {
    enumerable: false,

    get() {
      return ctx.$ref;
    }

  });
  Reflect.defineProperty($entity, '$$dimensions', {
    enumerable: false,

    get() {
      return new Promise(resolve => {
        resolve(ctx.$ref.spined.redis.hkeys(`${o.id}.D`));
      });
    }

  });
  Reflect.defineProperty($entity, 'hasDimension$', {
    enumerable: false,
    value: async name => {
      const all = await ctx.$ref.spined.redis.hkeys(`${o.id}.D`);
      return all.includes(name);
    }
  });
  const $proxy = new Proxy($entity, _handler.$handler); // TODO rather define only at proxy level ??

  Reflect.defineProperty($entity, 'dump$', {
    enumerable: false,
    value: () => {
      console.dir($entity, {
        getters: true,
        depth: 2
      });
    }
  });
  Reflect.defineProperty($entity, 'hasTrait$', {
    enumerable: false,
    value: t => o.traits.includes(t)
  });
  Reflect.defineProperty($entity, 'addTrait$', {
    enumerable: false,
    value: async t => {
      if (t === 'SINGULARITY') throw new Error(`[spined] SINGULARITY is a reserved trait`);
      if (t === 'OMNISCIENT' && !ctx.$ref.isOmniscient) throw new Error(`[spined] current Reference Frame not omniscient`);
      if (!ctx.$ref.isUnbounded) throw new Error(`[spined] current Reference Frame not unbounded`);
      await ctx.objectifier.storeEntity({ ...o,
        traits: [...o.traits, t]
      });
      return true;
    }
  });
  Reflect.defineProperty($entity, 'delTrait$', {
    enumerable: false,
    value: async t => {
      if (t === 'SINGULARITY') throw new Error(`[spined] SINGULARITY is a reserved trait`);
      if (t === 'OMNISCIENT' && !ctx.$ref.isOmniscient) throw new Error(`[spined] current Reference Frame not omniscient`);
      if (!ctx.$ref.isUnbounded) throw new Error(`[spined] current Reference Frame not unbounded`);
      await ctx.objectifier.storeEntity({ ...o,
        traits: o.traits.filter(x => x !== t)
      });
      return true;
    }
  });
  Reflect.defineProperty($entity, 'index$', {
    enumerable: false,
    value: obj => getIndex(obj)
  }); // TODO $$dimension => look in all dimensions of the reference frames

  Reflect.defineProperty($entity, 'dimension$', {
    enumerable: false,
    value: (0, _helpers.getDimension)(ctx, o)
  });
  Reflect.defineProperty($entity, 'assignDimension$', {
    enumerable: false,
    value: async (name, {
      schema,
      indexes = []
    } = {}) => {
      const dimNames = new Set(await ctx.$ref.spined.redis.hkeys(`${o.id}.D`));
      if (dimNames.has(name)) throw new Error(`[spined] ${o.id} already has a dimension '${name}'`);
      const id = (0, _helpers.makeId)();
      const dimO = await ctx.objectifier.storeDimension({
        id,
        name,
        meta: indexes.length ? ['INDEXED'] : [],
        schema,
        entityId: o.id
      });
      await ctx.$ref.spined.redis.hset(`${o.id}.D`, name, id);
      await Promise.all(indexes.map(identifier => {
        const {
          canonic
        } = (0, _helpers.getIndexParams)(identifier);
        return ctx.$ref.spined.redis.sadd(`${id}.I`, canonic);
      }));
      return (0, _dimension.default)(ctx, dimO);
    }
  });
  Reflect.defineProperty($entity, 'revokeDimension$', {
    enumerable: false,
    value: async name => {
      throw new Error(`[spined] revokeDimension not implemented yet`);
    }
  });
  Reflect.defineProperty($entity, 'update$', {
    enumerable: false,
    value: (0, _handler.update$)(ctx, o, $proxy)
  });
  Reflect.defineProperty($entity, 'duplicate$', {
    enumerable: false,
    value: (0, _handler.duplicate$)(ctx, o, $proxy)
  });
  Reflect.defineProperty($entity, 'delete$', {
    enumerable: false,
    value: (0, _handler.delete$)(ctx, o, $entity, $proxy)
  }); // XXX move to handler ...

  Reflect.defineProperty($entity, 'inc$', {
    enumerable: false,
    value: async name => {
      if (o.traits.includes('IMMUTABLE')) throw new Error(`[spined] cannot update an IMMUTABLE Entity`);
      if (!o.attributes[name]) throw new Error(`[spined] ${o.id} has no attribute ${name}'`);
      await ctx.objectifier.storeEntity({ ...o,
        attributes: { ...o.attributes,
          [name]: o.attributes[name] + 1
        }
      });
      return o.attributes[name];
    }
  });

  if (ctx.binding && ctx.binding.name) {
    var _ctx$binding;

    Object.keys((0, _helpers.getExtend)(ctx.$ref, (_ctx$binding = ctx.binding) === null || _ctx$binding === void 0 ? void 0 : _ctx$binding.name, 'getters')).forEach(name => {
      var _ctx$binding2;

      Reflect.defineProperty($entity, name, {
        enumerable: true,
        get: (0, _helpers.getExtend)(ctx.$ref, (_ctx$binding2 = ctx.binding) === null || _ctx$binding2 === void 0 ? void 0 : _ctx$binding2.name, 'getters', name)(ctx.$ref, $proxy)
      });
    });
  }

  Reflect.defineProperty($entity, 'get$', {
    enumerable: false,
    value: name => {
      if (o.traits.includes('INVISIBLE')) throw new Error(`[spined] cannot read an INVISIBLE Entity`);
      return typeof o.attributes[name] === 'object' ? JSON.parse(JSON.stringify(o.attributes[name])) : o.attributes[name];
    }
  });

  if (ctx.binding && ctx.binding.name) {
    var _ctx$binding3;

    Object.keys((0, _helpers.getExtend)(ctx.$ref, (_ctx$binding3 = ctx.binding) === null || _ctx$binding3 === void 0 ? void 0 : _ctx$binding3.name, 'methods')).forEach(name => {
      Reflect.defineProperty($entity, name, {
        enumerable: true,
        value: (...args) => {
          var _ctx$binding4;

          return (0, _helpers.getExtend)(ctx.$ref, (_ctx$binding4 = ctx.binding) === null || _ctx$binding4 === void 0 ? void 0 : _ctx$binding4.name, 'methods', name)(ctx.$ref, $proxy)(...args);
        }
      });
    });
  }
  /* XXX TODO dynamic model to be provided only with special starting option?
  Reflect.defineProperty($entity, 'addMethod$', {
    enumerable: false,
    value: async (name, fct) => {
      Reflect.defineProperty($entity, name, {
        enumerable: false,
        value: (...args) => fct(ctx.$ref, $proxy)(...args)
      })
    }
  })
  */


  return $proxy;
};

var _default = Entity;
exports.default = _default;
//# sourceMappingURL=entity.js.map