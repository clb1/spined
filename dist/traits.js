"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const SINGULARITY = 0b000001; // directly accessible

const OMNISCIENT = 0b000010; // allow to define new reference frame in reff

const UNBOUNDED = 0b000100; // allow traits change if reff

const PERPETUAL = 0b001000; // cannot be deleted thru spined

const IMMUTABLE = 0b010000; // cannot be updated thru spined

const INVISIBLE = 0b100000; // cannot be read thru spined

const TRAITS = {
  SINGULARITY,
  OMNISCIENT,
  UNBOUNDED,
  PERPETUAL,
  IMMUTABLE,
  INVISIBLE
};
var _default = TRAITS;
exports.default = _default;
//# sourceMappingURL=traits.js.map