const sqlite3 = require('sqlite3')
const IORedis = require('ioredis')
const Spined = require('../dist/index.js')

const redis = new IORedis({ port: 6377, db: 2 })

const sp = Spined.default({
  redis,
  options: {
    overloadTimestamp: true
  }
})

const run = async () => {
  /*
    TODO README
    cd tmp/sakila/sqlite-sakila-db/
    ./recreate_sakila_sqlite.sh
    export SAKILA_SQLLITE=./tmp/sakila/sqlite-sakila-db/sakila.db

   */

  const salika = await sp.createSingularity({})
  const Actor = await salika.assignDimension$('Actor', {
    indexes: [
      'U:actor_id',
      'S:last_name',
      'S: first_name & " " & last_name',
      'C: $$createdAt'
    ]
  })
  const Country = await salika.assignDimension$('Country', {
    indexes: ['U:country_id', 'U:country']
  })
  const City = await salika.assignDimension$('Cities', {
    indexes: ['U:city_id']
  })
  const Address = await salika.assignDimension$('Address', {
    indexes: ['U:address_id']
  })

  const db = new sqlite3.Database(process.env.SAKILA_SQLLITE)

  db.each('SELECT * FROM actor', function (err, row) {
    console.log(err, row)
    Actor.createEntity(row)
  })
  db.each('SELECT * FROM country', function (err, row) {
    console.log(err, row)
    Country.createEntity(row)
  })
  db.each('SELECT * FROM city', function (err, row) {
    console.log(err, row)
    City.createEntity(row)
  })
  db.each('SELECT * FROM address', function (err, row) {
    console.log(err, row)
    Address.createEntity(row)
  })

  // redis-cli -h {source_redis_address} -p 6379 -a {password} --rdb {output.rdb}
}

run()
