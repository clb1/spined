const nearley = require('nearley')
const grammar = require('./grammar.js')
const IORedis = require('ioredis')

const Spined = require('../dist/index.js')

// instead inject Spined from outside ?
// sepate sql engine & cli !

const redis = new IORedis({ port: 6377, db: 7 })
const db = Spined.default({
  redis,
  options: {
    overloadTimestamp: true
  }
})

let rf

// https://github.com/kach/nearley/blob/master/examples/json.ne

const parse = async phrase => {
  try {
    console.log(`receive *${phrase}*`)

    const parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar))
    parser.feed(phrase)

    if (parser.results.length > 1) {
      console.log('ambiguous grammar!')
    }
    console.log(parser.results) // [[[[ "foo" ],"\n" ]]]

    switch (parser.results[0][0] && parser.results[0][0].method) {
      case 'referenceFrame':
        if (parser.results[0][0].entity) {
          console.log('world!')
        } else {
          console.log(rf.name)
        }
        break
      case 'listDimension$': {
        // give all dimensions of the reference frames
        const x = await rf.listDimension$()
        for (const d of x) {
          console.log(d.name)
        }

        break
      }
      case 'select':
        console.log('world!')
        break
      default:
        console.log(parser.results) // [[[[ "foo" ],"\n" ]]]
        break
    }

    // const sg = await db.referenceFrame(sgUuid);
  } catch (e) {
    console.log(e)
  }
}

const run = async () => {
  // TODO
  rf = await db.referenceFrame('2273b2a5-8628-4056-a5d2-be79c139c405')

  const readline = require('readline')
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'SQL> '
  })

  rl.prompt()

  rl.on('line', async line => {
    await parse(line.trim() + '\n')
    rl.prompt()
  }).on('close', () => {
    console.log('Spin the world for a great today!')
    process.exit(0)
  })
}

run()
