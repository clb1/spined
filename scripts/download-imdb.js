const IORedis = require('ioredis')
const Spined = require('../dist/index.js')

const redis = new IORedis({ port: 6377, db: 2 })

const sp = Spined.default({ redis })

const fs = require('fs')
const readline = require('readline')

// const util = require('util')
// const streamPipeline = util.promisify(require('stream').pipeline)
// const fetch = require('node-fetch')

const zlib = require('zlib')
const gunzip = zlib.createGunzip()

let count = 0

// Documentation for these data files can be found on http://www.imdb.com/interfaces/

const files = [
  'name.basics.tsv.gz',
  'title.akas.tsv.gz',
  'title.basics.tsv.gz',
  'title.crew.tsv.gz',
  'title.episode.tsv.gz',
  'title.principals.tsv.gz',
  'title.ratings.tsv.gz'
]

/*
const base = 'https://datasets.imdbws.com/'
const download = async () => {
  for (const f of files) {
    console.log(`downloading ${f} from imdb...`)
    const response = await fetch(base + f)
    if (!response.ok)
      throw new Error(`unexpected response ${response.statusText}`)
    await streamPipeline(response.body, fs.createWriteStream('./tmp/' + f))
  }
} */

const ImportIMDB = {
  'name.basics.tsv.gz': imdb => async line => {
    // name.basics.tsv.gz – Contains the following information for names:

    // nconst (string) - alphanumeric unique identifier of the name/person
    // primaryName (string)– name by which the person is most often credited
    // birthYear – in YYYY format
    // deathYear – in YYYY format if applicable, else '\N'
    // primaryProfession (array of strings)– the top-3 professions of the person
    // knownForTitles (array of tconsts) – titles the person is known for

    if (count > 500000) return
    // test A with and without requesting dim
    const dim = await imdb.dimension$('Person')
    const fields = line.split('\t')
    dim.createEntity({
      nconst: fields[0],
      primaryName: fields[1],
      birthYear: fields[2],
      deathYear: fields[3],
      primaryProfession: fields[4].split(','),
      knownForTitles: fields[5].split(',')
    })
    count++
  },

  'title.akas.tsv.gz': async line => {
    // title.akas.tsv.gz - Contains the following information for titles:
    // titleId (string) - a tconst, an alphanumeric unique identifier of the title
    // ordering (integer) – a number to uniquely identify rows for a given titleId
    // title (string) – the localized title
    // region (string) - the region for this version of the title
    // language (string) - the language of the title
    // types (array) - Enumerated set of attributes for this alternative title.
    //      One or more of the following: "alternative", "dvd", "festival", "tv", "video", "working",
    //      "original", "imdbDisplay". New values may be added in the future without warning
    // attributes (array) - Additional terms to describe this alternative title, not enumerated
    // isOriginalTitle (boolean) – 0: not original title; 1: original title
    // const dim = await imdb.dimension$('Person')
    // const fields = line.split("a")
    // console.log(fields[0])
  }
}

const parse = async (file, handler) => {
  const readInterface = readline.createInterface({
    input: fs.createReadStream('./tmp/' + file).pipe(gunzip),
    console: false
  })
  readInterface.on('line', handler)
  readInterface.on('end', () => console.log('END'))
}

const run = async () => {
  const imdb = await sp.createSingularity({})
  imdb.assignDimension$('Person')

  parse(files[0], ImportIMDB[files[0]](imdb))
}

run()
