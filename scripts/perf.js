/* global */

import Redis from 'ioredis'

import Objectifier from '../src/objectifier'

import { v4 as uuidv4, v5 as uuidv5 } from 'uuid'

const redis = new Redis(process.env.REDIS_PORT)

// eslint-disable-next-line max-len
// https://medium.com/profil-software-blog/database-compare-sql-vs-nosql-mysql-vs-postgresql-vs-redis-vs-mongodb-3da5f41c31b5

// const inMemoryMax = 1
const inMemoryMax = 10000
const objectifier = Objectifier({ redis, inMemoryMax })

const makeRawEntity = (i, uuid) =>
  objectifier.storeEntity({
    uuid: uuid || uuidv4(),
    attributes: {
      i,
      name: 'Felicia Haag',
      username: 'Montana97',
      email: 'Carlos.King@yahoo.com',
      address: {
        streetA: 'Orie Bridge',
        streetB: '0902 Mercedes River',
        streetC: '2179 Josephine Expressway Suite 022',
        streetD: 'Apt. 922',
        city: 'Jazmynfurt',
        state: 'Michigan',
        country: 'United Arab Emirates',
        zipcode: '71364-0861',
        geo: { lat: '-57.8143', lng: '-172.2001' }
      },
      phone: '404-531-9867',
      website: 'coty.com',
      company: {
        name: 'Heidenreich, MacGyver and Tremblay',
        catchPhrase: 'Mandatory 3rd generation toolset',
        bs: 'scalable reintermediate action-items'
      }
    }
  })

const create = async () => {
  // const maxHeap = 0
  const start = new Date().getTime()

  console.log(
    `checking 1 million record create with inMemoryMax=${inMemoryMax}`
  )

  for (let i = 0; i < 1000000; i++) {
    await makeRawEntity(i)
    // const used = process.memoryUsage().heapUsed;
    // if (used > maxHeap) maxHeap = used,
    // console.log(`${i} `, process.memoryUsage().heapUsed) // maxHeap, used);
  }

  const end = new Date().getTime()

  console.log(objectifier.status(), `${end - start} `)

  // effect of inMemoryMax on 100k creation
  // 1      => 10324
  // 10     => 10143
  // 100    => 10141
  // 1000   => 10186
  // 10000  => 10293
  // 100000 => 10318

  // ~ 0.075s per 600 record
  // 1million creation / 1m inMemoryMax => ~110seconds (8 create per ms)
  // heap goes to 439mb
  // 1million creation / 10k inMemoryMax => ~110seconds
  // heap stays around 33mb
}

const select = async () => {
  const namespace = uuidv4()
  const records = 10000
  const test = 5000
  const random = []

  console.log('1. create 10k record to be used by select')

  for (let i = 0; i < records; i++) {
    await makeRawEntity(i, uuidv5(i + '', namespace))
  }

  for (let i = 0; i < test; i++) {
    random.push(
      uuidv5(Math.floor(Math.random() * Math.floor(records)) + '', namespace)
    )
  }

  const start = new Date().getTime()
  for (let i = 0; i < test; i++) {
    await objectifier.retrieveEntity(random[i])
  }
  const end = new Date().getTime()

  console.log(objectifier.status(), `${end - start} `)

  // 5000 select with inMemory => 9ms       ~0.0018ms/record
  // 5000 select with no inMemory => 462ms  ~0.0924ms/record
}

const reuse = async () => {
  // with objectifier 100 =>  9000 (97% objectifier hit)
  // with objectifier 10  => 12113 (20% objectifier hit)
  // with objectifier 1   => 15408

  const saved = {}
  const pct = { true: 0, false: 0 }

  const start = new Date().getTime()

  for (let i = 0; i < 5000; i++) {
    const letter = String.fromCharCode(64 + Math.ceil(Math.random() * 26))

    if (!saved[letter] || Math.random() * 10 > 5) {
      let o = await makeRawEntity(i)
      saved[letter] = o
      o = null
    } else {
      let o = await objectifier.retrieveEntity(saved[letter].uuid)
      console.log(`${i} ${letter} ${o.traits}`, process.memoryUsage().heapUsed) // maxHeap, used);
      if (saved[letter] === o) {
        pct.true = pct.true + 1
      } else {
        pct.false = pct.false + 1
      }
      o = null
    }
  }

  const end = new Date().getTime()

  console.log(objectifier.status(), `${end - start} `, {
    ok: pct.true / (pct.true + pct.false),
    ko: pct.false / (pct.true + pct.false)
  })
}

const run = async () => {
  await create()

  await select()

  if (process.argv[2]) await reuse()

  // redis.quit()
}

run()
