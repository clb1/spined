const util = require('util')
const sqlite3 = require('sqlite3')
const createCsvWriter = require('csv-writer').createObjectCsvWriter
const faker = require('faker')

const run = async () => {
  const db = new sqlite3.Database(process.env.SAKILA_SQLLITE)
  const all = util.promisify((...args) => db.each(...args))

  const FILMS = []
  await all(
    'SELECT * FROM film, language WHERE film.language_id=language.language_id',
    function (err, row) {
      console.log(err, row)
      FILMS.push(row)
      row.score = Math.floor(Math.random() * 100)
    }
  )

  await createCsvWriter({
    path: './samples/film.csv',
    header: [
      { id: 'title', title: 'title' },
      { id: 'description', title: 'description' },
      { id: 'length', title: 'length' },
      { id: 'rating', title: 'rating' },
      { id: 'score', title: 'score' },
      { id: 'release_year', title: 'releaseYear' },
      { id: 'name', title: 'language' }
    ]
  }).writeRecords(FILMS)

  const ACTORS = []
  await all('SELECT * FROM actor', (err, row) => {
    row.email = faker.internet.email()
    row.phone = faker.phone.phoneNumber()
    console.log(err, row)
    ACTORS.push(row)
  })

  await createCsvWriter({
    path: './samples/actor.csv',
    header: [
      { id: 'first_name', title: 'firstName' },
      { id: 'last_name', title: 'lastName' },
      { id: 'email', title: 'email' },
      { id: 'phone', title: 'phone' }
    ]
  }).writeRecords(ACTORS)

  console.log('end')
}

run()
