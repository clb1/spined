import Redis from 'ioredis'
import { makeId, isId } from '../src/helpers'

export { makeId, isId }

export const redis = new Redis({
  lazyConnect: true,
  port: process.env.REDIS_PORT,
  maxRetriesPerRequest: 0
})
redis.on('error', err => {
  if (err.code === 'ECONNREFUSED') {
    throw new Error('wrong env.REDIS_PORT?')
  }
})

// uuid => /^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$/
export const expectedId = expect.stringMatching(/^[A-Za-z0-9_-]{21}$/)

export const expectedISO8601 = expect.stringMatching(
  // eslint-disable-next-line max-len
  /^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])-(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:\.\d{1,9})?(?:Z|[+-][01]\d:[0-5]\d)$/
)

export const wait = ms =>
  new Promise(resolve => setTimeout(() => resolve(), ms))
