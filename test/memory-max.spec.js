import Redis from 'ioredis'
import { makeId } from '../src/helpers'

import Objectifier from '../src/objectifier'

const redis = new Redis(process.env.REDIS_PORT)

const inMemoryMax = 3
const objectifier = Objectifier({ redis, inMemoryMax })

afterAll(() => redis.quit())

let count = 0

const makeRawEntity = () => {
  count = count + 1
  return objectifier.storeEntity({
    id: makeId(),
    attributes: { count },
    traits: []
  })
}

describe('store an Entity in the objectifier', () => {
  const all = []

  test('should return a raw entity', async () => {
    const first = await makeRawEntity()

    for (let i = 0; i < inMemoryMax * 100; i++) {
      all.push(await makeRawEntity())

      if (i < 2) {
        expect(objectifier.retrieveEntity(first.id)).resolves.toBe(first)
      } else {
        expect(objectifier.retrieveEntity(first.id)).not.toBe(first)
      }
    }

    // console.log(all);

    expect(all[0]).toHaveProperty('attributes')
  })
})
