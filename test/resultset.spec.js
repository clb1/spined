import Spined from '../src/'

import { redis } from './utils'
import { mini } from './data-samples'

const sp = Spined({ redis })
let sample = {}

beforeAll(async () => {
  await redis.connect()
  sample = await mini(sp)
  return true
})

afterAll(() => redis.quit())

describe('Misc', () => {
  xtest('rs.iterate(cursor)', async () => {
    const { Film } = sample

    const ByScore = Film.index('C:Num:score')
    expect(sp.isIndex(ByScore)).toBe(true)

    for await (const e of Film.iterate('C:Num:score')) {
      console.log(` ${e.title} ${e.score} `)
    }
  })

  xtest('rs.order(cursor).iterate()', async () => {
    const { Film } = sample

    const ByScore = Film.index('C:Num:score')
    expect(sp.isIndex(ByScore)).toBe(true)

    for await (const e of Film.filter('$contains(title, "AUR")')
      .order('C:Num:score')
      .iterate()) {
      console.log(` ${e.title} ${e.score} `)
    }
  })

  xtest('rs.order(cursor).connection()', async () => {
    const { Film } = sample

    const ByScore = Film.index('C:Num:score')
    expect(sp.isIndex(ByScore)).toBe(true)

    const connection = await Film.order('C:Num:score').connection({ last: 10 })
    // console.log ( connection )

    for await (const edge of connection.edges) {
      console.log(edge.cursor, ` ${edge.entity.title}`, edge.entity.$$json)
    }
  })
})
