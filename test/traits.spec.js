import Redis from 'ioredis'

import Spined from '../src/'

const redis = new Redis(process.env.REDIS_PORT)

const sp = Spined({ redis })

afterAll(() => redis.quit())

const sgTraits = ['SINGULARITY', 'OMNISCIENT', 'UNBOUNDED']

for (const t of sgTraits) {
  describe(`${t} trait`, () => {
    test('exists after sp.createSingularity()', async () => {
      const sng = await sp.createSingularity()
      expect(sp.isEntity(sng)).toBe(true)
      expect(sng.hasTrait$(t)).toBe(true)
      expect(sng.$$traits).toContain(t)
    })

    test('does not exist after ent.duplicate$()', async () => {
      const sng = await sp.createSingularity()
      const ent = await sng.duplicate$()

      expect(sp.isEntity(ent)).toBe(true)
      expect(ent.hasTrait$(t)).toBe(false)
      expect(ent.$$traits).not.toContain(t)
    })

    test('does not exist after dim.createEntity()', async () => {
      const sng = await sp.createSingularity()
      const dim = await sng.assignDimension$('test')
      const ent = await dim.createEntity({ e: 1, s: 0 })

      expect(sp.isEntity(ent)).toBe(true)
      expect(ent.hasTrait$(t)).toBe(false)
      expect(ent.$$traits).not.toContain(t)
    })
  })
}

describe('SINGULARITY trait', () => {
  test('cannot be added with ent.addTrait$()', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')
    const ent = await dim.createEntity({ e: 1, s: 0 })

    expect(ent.addTrait$('SINGULARITY')).rejects.toThrow(
      '[spined] SINGULARITY is a reserved trait'
    )
  })

  test('cannot be removed with ent.delTrait$()', async () => {
    const sng = await sp.createSingularity()

    expect(sng.delTrait$('SINGULARITY')).rejects.toThrow(
      '[spined] SINGULARITY is a reserved trait'
    )
  })
})

describe('OMNISCIENT trait', () => {
  test('can be modified with ent.add/delTrait$() in unbounded reference frame', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')
    const ent = await dim.createEntity({ e: 1, s: 0 })

    expect(ent.hasTrait$('OMNISCIENT')).toBe(false)
    expect(await ent.addTrait$('OMNISCIENT')).toBe(true)
    expect(ent.hasTrait$('OMNISCIENT')).toBe(true)
    expect(await ent.delTrait$('OMNISCIENT')).toBe(true)
    expect(ent.hasTrait$('OMNISCIENT')).toBe(false)
  })

  test('can notbe modified with ent.add/delTrait$() in bounded reference frame', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')
    let ent = await dim.createEntity({ e: 1, s: 0 })
    const nof = await sp.defineReferenceFrame(ent)
    ent = nof.default

    expect(ent.addTrait$('OMNISCIENT')).rejects.toThrow(
      '[spined] current Reference Frame not omniscient'
    )
    expect(ent.delTrait$('OMNISCIENT')).rejects.toThrow(
      '[spined] current Reference Frame not omniscient'
    )
  })
})

const traits = ['UNBOUNDED', 'PERPETUAL', 'IMMUTABLE', 'INVISIBLE']

for (const t of traits) {
  describe(`${t} trait`, () => {
    test('can be modified with ent.add/delTrait$() if unbounded reference frame', async () => {
      const sng = await sp.createSingularity()
      const dim = await sng.assignDimension$('test')
      const ent = await dim.createEntity({ e: 1, s: 0 })

      expect(ent.hasTrait$(t)).toBe(false)
      expect(await ent.addTrait$(t)).toBe(true)
      expect(ent.hasTrait$(t)).toBe(true)
      expect(await ent.delTrait$(t)).toBe(true)
      expect(ent.hasTrait$(t)).toBe(false)
    })

    test('can not be modified with ent.add/delTrait$() if non unbounded reference frame', async () => {
      const sng = await sp.createSingularity()
      const dim = await sng.assignDimension$('test')
      let ent = await dim.createEntity({ e: 1, s: 0 })
      const nof = await sp.defineReferenceFrame(ent)
      ent = nof.default

      expect(ent.addTrait$(t)).rejects.toThrow(
        '[spined] current Reference Frame not unbounded'
      )
      expect(ent.delTrait$(t)).rejects.toThrow(
        '[spined] current Reference Frame not unbounded'
      )
    })
  })
}

test('PERPETUAL *Entity* can not use ent.delete$()', async () => {
  const sng = await sp.createSingularity()
  const dim = await sng.assignDimension$('test')
  const ent = await dim.createEntity({ e: 1, s: 0 })

  expect(await ent.addTrait$('PERPETUAL')).toBe(true)
  expect(ent.hasTrait$('PERPETUAL')).toBe(true)

  return expect(ent.delete$()).rejects.toThrow(
    '[spined] cannot delete a PERPETUAL Entity'
  )
})

test('IMMUTABLE *Entity* can not use ent.update$()', async () => {
  const sng = await sp.createSingularity()
  const dim = await sng.assignDimension$('test')
  const ent = await dim.createEntity({ e: 1, s: 0 })

  expect(await ent.addTrait$('IMMUTABLE')).toBe(true)
  expect(ent.hasTrait$('IMMUTABLE')).toBe(true)

  expect(ent.update$({ a: true })).rejects.toThrow(
    '[spined] cannot update an IMMUTABLE Entity'
  )

  return expect(ent.inc$('e')).rejects.toThrow(
    '[spined] cannot update an IMMUTABLE Entity'
  )
})

test('INVISIBLE *Entity* data object cannot be fetched', async () => {
  const sng = await sp.createSingularity()
  const dim = await sng.assignDimension$('test')
  const ent = await dim.createEntity({ e: 1, s: 0 })

  expect(await ent.addTrait$('INVISIBLE')).toBe(true)
  expect(ent.hasTrait$('INVISIBLE')).toBe(true)

  expect(ent.duplicate$()).rejects.toThrow(
    '[spined] cannot duplicate an INVISIBLE Entity'
  )

  expect(() => ent.get$('e')).toThrow(
    '[spined] cannot read an INVISIBLE Entity'
  )

  expect(() => ent.$$jon).toThrow('[spined] cannot read an INVISIBLE Entity')

  return expect(() => ent.$$jon$$).toThrow(
    '[spined] cannot read an INVISIBLE Entity'
  )
})
