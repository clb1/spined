import Redis from 'ioredis'

import Spined from '../src/'

import { expectedId } from './utils'

const redis = new Redis(process.env.REDIS_PORT)

const spined = Spined({ redis })

afterAll(() => redis.quit())

describe('On a new entity', () => {
  test('deep equality', async () => {
    const Entity = await spined.createSingularity()
    const EntityBis = (await spined.referenceFrame(Entity.$$id)).default

    expect(Entity.$$id).toEqual(expectedId)
    expect(EntityBis.$$id).toEqual(expectedId)
    expect(EntityBis.$$id).toBe(Entity.$$id)
  })

  xtest('addTrait', async () => {
    const Entity = await spined.createSingularity()
    const EntityFetchBefore = await spined.referenceFrame(Entity)
    await Entity.addTrait$('UNBOUNDED')
    const EntityFetchAfter = await spined.referenceFrame(Entity)

    expect(Entity.traits$).toContain('UNBOUNDED')
    expect(EntityFetchBefore.traits$).toContain('UNBOUNDED')
    expect(EntityFetchAfter.traits$).toContain('UNBOUNDED')
  })

  xtest('deleteTrait', async () => {
    const Entity = await spined.createSingularity()
    const EntityFetchBefore = await spined.referenceFrame(Entity)
    await Entity.deleteTrait$('UNBOUNDED')
    const EntityFetchAfter = await spined.referenceFrame(Entity)

    expect(Entity.traits$).not.toContain('UNBOUNDED')
    expect(EntityFetchBefore.traits$).not.toContain('UNBOUNDED')
    expect(EntityFetchAfter.traits$).not.toContain('UNBOUNDED')
  })

  // // count is same as loop
  // let count = 0;
  // const r3 = await Contract.search();
  // for await (const r of r3) {
  //   db.dump(r);
  //   count += 1;
  // }
  // console.log('count', count, await Contract.count());
})
