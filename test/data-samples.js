const csv = require('csv-parser')
const fs = require('fs')

const importCSV = (dim, file) =>
  new Promise(resolve => {
    const lines = []
    fs.createReadStream(file)
      .pipe(csv())
      .on('data', data => lines.push(data))
      .on('end', async () => {
        for await (const l of lines) {
          // if (l.title === 'ADAPTATION HOLES') {
          await dim.createEntity(l)
          // }
        }
        return resolve()
      })
  })

// create a mini Film/Actor database to test indexes
export const mini = async sp => {
  const mini = await sp.createSingularity()

  const Film = await mini.assignDimension$('Film', {
    indexes: ['S:title', 'C:Num:score', 'S: rating', 'C:$$createdAt']
  })
  await importCSV(Film, './samples/film.csv')

  const Actor = await mini.assignDimension$('Actor', {
    indexes: [
      'U:email',
      'C:email',
      'S: firstName & " " & lastName',
      'C: $$createdAt'
    ]
  })
  await importCSV(Actor, './samples/actor.csv')

  return { sp, Film, Actor }
}
