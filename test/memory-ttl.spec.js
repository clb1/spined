import Redis from 'ioredis'
import { makeId } from '../src/helpers'

import Objectifier from '../src/objectifier'

const redis = new Redis(process.env.REDIS_PORT)

const inMemoryMax = 100
const defaultInMemoryTtl = 1000 // 1 second
const objectifier = Objectifier({ redis, inMemoryMax, defaultInMemoryTtl })

afterAll(() => redis.quit())

const makeRawEntity = () =>
  objectifier.storeEntity({
    id: makeId(),
    attributes: { field: true },
    traits: []
  })

describe('store an Entity in the objectifier', () => {
  test('should return a raw entity', async () => {
    const o = await makeRawEntity()

    const cached1 = await objectifier.retrieveEntity(o.id)
    const cached2 = await objectifier.retrieveEntity(o.id)

    expect(o).toBe(cached1)
    expect(o).toBe(cached2)

    await new Promise(resolve =>
      setTimeout(() => resolve(), defaultInMemoryTtl)
    )

    const fresh1 = await objectifier.retrieveEntity(o.id)
    const fresh2 = await objectifier.retrieveEntity(o.id)

    expect(o.attributes).toStrictEqual(fresh1.attributes)
    expect(fresh1).toBe(fresh2)

    expect(o).not.toBe(fresh1)
    expect(o).not.toBe(fresh2)
  })
})
