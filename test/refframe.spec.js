import Redis from 'ioredis'
import Spined from '../src/'

const redis = new Redis(process.env.REDIS_PORT)

const spined = Spined({ redis })

afterAll(() => redis.quit())

// ref.$('staff.Account#index').find(id)!!!
// ref('staff.*').find(id)!!!

// find id only on dimension

describe('default reference frame', () => {
  test('from a new singularity', async () => {
    const S = await spined.createSingularity({ a: 1, b: 2 })
    const R1 = S.$$ref.default

    expect(spined.isReferenceFrame(S.$$ref)).toBe(true)
    expect(S.$$ref.isOmniscient).toBe(true)
    expect(S.$$ref.aliases).toStrictEqual(['default', '0'])

    expect(R1.$$id).toEqual(S.$$id)
    expect(R1.$$json$$).toStrictEqual(S.$$json$$)

    // duplicate has the same reference frame

    const D = await S.duplicate$()
    const R2 = D.$$ref.default

    expect(spined.isReferenceFrame(D.$$ref)).toBe(true)
    expect(D.$$ref.isOmniscient).toBe(true)
    expect(D.$$ref.aliases).toStrictEqual(['default', '0'])

    expect(R2.$$id).toEqual(S.$$id)
    expect(R2.$$json$$).toStrictEqual(S.$$json$$)

    // recheck duplicate is not a singularity
    expect(spined.referenceFrame(D.$$id)).rejects.toThrow(
      '[spined] not a singularity, cannot retreive reference frame'
    )
  })

  test('from an existing singularity', async () => {
    const n = await spined.createSingularity({ c: 1, d: 2 })
    const id = n.$$id

    const frame = await spined.referenceFrame(id)
    expect(spined.isReferenceFrame(frame)).toBe(true)
    expect(frame.aliases).toStrictEqual(['default', '0'])

    const R1 = frame.default

    // duplicate has the same reference frame

    const D = await R1.duplicate$()
    const R2 = D.$$ref.default

    expect(spined.isReferenceFrame(D.$$ref)).toBe(true)
    expect(D.$$ref.aliases).toStrictEqual(['default', '0'])
    expect(R2.$$id).toEqual(R1.$$id)
    expect(R2.$$json$$).toStrictEqual(R1.$$json$$)

    // recheck duplicate is not a singularity
    expect(spined.referenceFrame(D.$$id)).rejects.toThrow(
      '[spined] not a singularity, cannot retreive reference frame'
    )
  })

  test('from one or two unpriviledge entities', async () => {
    const start = await spined.createSingularity({ e: 1, f: 2 })
    const dim = await start.assignDimension$('test')

    const e1 = await dim.createEntity({ e: 1, s: 0 })
    const e2 = await dim.createEntity({ e: 2, s: 0 })

    expect(spined.isReferenceFrame(e1.$$ref)).toBe(true)
    expect(e1.$$json).toStrictEqual({ e: 1, s: 0 })
    expect(spined.referenceFrame(e1.$$id)).rejects.toThrow(
      '[spined] not a singularity, cannot retreive reference frame'
    )
    expect(spined.referenceFrame(e2.$$id)).rejects.toThrow(
      '[spined] not a singularity, cannot retreive reference frame'
    )

    const frame1 = await spined.defineReferenceFrame(e1)
    expect(spined.isReferenceFrame(frame1)).toBe(true)
    expect(frame1.aliases).toStrictEqual(['default', '0'])
    expect(frame1.default.$$id).toEqual(e1.$$id)
    expect(frame1.default.$$json$$).toEqual(e1.$$json$$)
    expect(frame1[0].$$id).toEqual(e1.$$id)
    expect(frame1[0].$$json$$).toEqual(e1.$$json$$)

    expect(spined.defineReferenceFrame(frame1[0])).rejects.toThrow(
      '[spined] need omniscient reference frame to define a new one'
    )

    const frame2 = await spined.defineReferenceFrame(e1, e2)
    expect(spined.isReferenceFrame(frame2)).toBe(true)
    expect(frame2.aliases).toStrictEqual(['default', '0', '1'])
    expect(frame2.default.$$id).toEqual(e1.$$id)
    expect(frame2.default.$$json$$).toEqual(e1.$$json$$)
    expect(frame2[0].$$id).toEqual(e1.$$id)
    expect(frame2[0].$$json$$).toEqual(e1.$$json$$)
    expect(frame2[1].$$id).toEqual(e2.$$id)
    expect(frame2[1].$$json$$).toEqual(e2.$$json$$)

    expect(spined.defineReferenceFrame(frame2[0], frame2[1])).rejects.toThrow(
      '[spined] need omniscient reference frame to define a new one'
    )

    // explicit aliases syntax

    const frame3 = await spined.defineReferenceFrame({ one: e1, two: e2 })
    expect(spined.isReferenceFrame(frame3)).toBe(true)
    expect(frame3.aliases).toStrictEqual(['default', 'one', 'two'])
    expect(frame3.default.$$id).toEqual(e1.$$id)
    expect(frame3.default.$$json$$).toEqual(e1.$$json$$)
    expect(frame3.one.$$id).toEqual(e1.$$id)
    expect(frame3.one.$$json$$).toEqual(e1.$$json$$)
    expect(frame3.two.$$id).toEqual(e2.$$id)
    expect(frame3.two.$$json$$).toEqual(e2.$$json$$)
  })

  // ref frame comparaison
})
