import Spined from '../src/'

import { redis, expectedId } from './utils'
import { mini } from './data-samples'

const sp = Spined({ redis })
let sample = {}

beforeAll(async () => {
  await redis.connect()
  sample = await mini(sp)
  return true
})

afterAll(() => redis.quit())

describe('Incorrect index', () => {
  test('X:$$created', async () => {
    const { Actor } = sample
    expect(sp.isDimension(Actor)).toBe(true)

    expect(() => Actor.index('X:$$created')).toThrow(
      "[spined] dimension has no index 'X:$$created'"
    )
  })
})

describe('Index type U:', () => {
  test('idx.find()', async () => {
    const { Actor } = sample

    const WithEmail = Actor.index('U:email')
    expect(sp.isIndex(WithEmail)).toBe(true)

    const email = 'Neoma.Gutmann94@hotmail.com'
    const entity = await WithEmail.find(email)
    expect(sp.isEntity(entity)).toBe(true)
    expect(entity.email).toBe(email)
    expect(entity.$$id).toEqual(expectedId)
  })
})

describe('Index type S:', () => {
  const result = []
  test('idx.count()', async () => {
    const { Film } = sample

    const PerRating = Film.index('S: rating')
    expect(sp.isIndex(PerRating)).toBe(true)

    const rating = 'R'
    expect(PerRating.count(rating)).resolves.toBe(195)
  })

  xtest('idx.search()', async () => {
    const { Film } = sample

    const PerRating = Film.index('S: rating')
    expect(sp.isIndex(PerRating)).toBe(true)

    const rating = 'PG-13'
    const search = await PerRating.filter(rating)
    for await (const e of search) result.push(e.title)
    expect(result.length).toBe(223)
    expect(result).toContain('UNDEFEATED DALMATIONS')
    expect(result).not.toContain('WISDOM WORKER')
  })

  xtest('idx.first()', async () => {
    const { Film } = sample

    const PerRating = Film.index('S: rating')
    expect(sp.isIndex(PerRating)).toBe(true)

    const rating = 'PG-13'
    const f = await PerRating.first(rating)
    expect(result).toContain(f.title)
  })
})

xdescribe('Index type C:', () => {
  xtest('idx.rank()', async () => {
    const { Film } = sample

    const PerTitle = Film.index('S:title')
    const ent = await PerTitle.first('CHARIOTS CONSPIRACY')
    expect(ent.score).toBe('85') // XXX sample problem

    const ByScore = Film.index('C:Num:score')
    expect(sp.isIndex(ByScore)).toBe(true)

    const rank = await ByScore.rank(ent.$$id)

    expect(rank).toBe(836)
  })

  test('idx.search() (Num)', async () => {
    const { Film } = sample

    const ByScore = Film.index('C:Num:score')
    expect(sp.isIndex(ByScore)).toBe(true)

    const scores = []
    const search = await ByScore.search()
    for await (const e of search) scores.push(e.score)

    return expect(scores).toEqual(
      [...scores].sort((a, b) => parseInt(a) - parseInt(b))
    )
  })

  test('idx.search() (AlphaNum)', async () => {
    const { Actor } = sample

    const ByEmail = Actor.index('C:email')
    expect(sp.isIndex(ByEmail)).toBe(true)

    const list = []
    const search = await ByEmail.search()
    for await (const e of search) list.push(e.email)

    // console.log(list.join('\n'));

    return expect(list).toEqual([...list].sort())
  })
})
