import Redis from 'ioredis'

import Spined from '../src/'

const redis = new Redis(process.env.REDIS_PORT)

const sp = Spined({ redis })

afterAll(() => redis.quit())

describe('ent.assignDimension$()', () => {
  test('return a new Dimension', async () => {
    const sng = await sp.createSingularity()
    expect(sng.$$dimensions).resolves.toStrictEqual([])

    expect(sng.dimension$('test0')).rejects.toThrow(
      `[spined] dimension 'test0' doesn't exist`
    )

    await sng.assignDimension$('test1')
    expect(sng.$$dimensions).resolves.toStrictEqual(['test1'])

    await sng.assignDimension$('test2')
    expect(sng.$$dimensions).resolves.toStrictEqual(['test1', 'test2'])

    expect(sng.dimension$('test1')).resolves.toMatchObject({ name: 'test1' })
    expect(sng.dimension$('test2')).resolves.toMatchObject({ name: 'test2' })

    const dim1 = await sng.dimension$('test1')
    expect(sp.isDimension(dim1)).toBe(true)

    const dim2 = await sng.dimension$('test2')
    return expect(sp.isDimension(dim2)).toBe(true)
  })
})

describe('dim.createEntity()', () => {
  test('return Entity inside Dimension', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')
    expect(sp.isDimension(dim)).toBe(true)

    const ent = await dim.createEntity({ l: [2, 7, 8], s: 'hello', t: true })
    expect(sp.isEntity(ent)).toBe(true)
    expect(ent.t).toBe(true)
    expect(dim.count()).resolves.toBe(1)

    expect((await dim.first()).$$json).toMatchObject({ s: 'hello' })
    expect(dim.has(ent)).resolves.toBe(true)
    expect(dim.has(ent.$$id)).resolves.toBe(true)
  })
})

describe('dim.retrieve()', () => {
  test('only retrieve Entity inside Dimension', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    await dim.createEntity({ l: [2, 7, 8], s: 'hello1', t: true })
    await dim.createEntity({ l: [3, 6, 8], s: 'hello2', t: false })
    const ent3 = await dim.createEntity({ l: [4, 5, 8], s: 'hello3', t: true })

    expect(dim.retrieve(sng.$$id)).rejects.toThrow(
      '[spined] can not retrieve this id'
    )

    const found = await dim.retrieve(ent3.$$id)
    expect(sp.isEntity(found)).toBe(true)
    expect(found.$$id).toBe(ent3.$$id)

    expect(dim.count()).resolves.toBe(3)
  })
})

describe('dim.all()', () => {
  test('get all Entities in a Dimension and iterate', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    await dim.createEntity({ l: [1, 2, 3] })
    await dim.createEntity({ l: [0, 4, 5] })
    await dim.createEntity({ l: [6, 7, 8] })
    expect(dim.count()).resolves.toBe(3)

    const all = dim.all()
    expect(sp.isResultset(all)).toBe(true)

    let result = []
    for await (const e of all.iterate()) {
      result = [...e.l, ...result]
    }
    expect(result.length).toBe(9)
    expect(result.sort()).toEqual([0, 1, 2, 3, 4, 5, 6, 7, 8])
  })
})

describe('Sugar syntax dimension methods', () => {
  test('dim.filter()', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    await dim.createEntity({ l: [2, 7, 8], s: 'hello1', t: true })
    await dim.createEntity({ l: [3, 6, 9], s: 'bye2', t: false })
    await dim.createEntity({ l: [4, 5, 8], s: 'hello3', t: true })
    await dim.createEntity({ l: [1, 5, 1], s: 'bye4', t: true })
    expect(dim.count()).resolves.toBe(4)

    const filtered = dim.filter('$contains(s, "hello")')
    expect(sp.isResultset(filtered)).toBe(true)

    const result = []
    for await (const e of filtered.iterate()) result.push(e.s)

    expect(result.length).toBe(2)
    expect(result).toContain('hello1')
    expect(result).toContain('hello3')
  })

  test('dim.first()', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    await dim.createEntity({ b: 1 })
    await dim.createEntity({ b: 2 })
    expect(dim.count()).resolves.toBe(2)

    expect(
      dim
        .all()
        .filter('b > 1')
        .first()
    ).resolves.toHaveProperty('b', 2)
    expect(dim.all().first('b > 1')).resolves.toHaveProperty('b', 2)
    return expect(dim.first('b > 1')).resolves.toHaveProperty('b', 2)
  })

  test('dim.ids()', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    const ent1 = await dim.createEntity({ 1: '' })
    const ent2 = await dim.createEntity({ 2: '' })
    const ent3 = await dim.createEntity({ 3: '' })

    const ids = await dim.ids()

    expect(ids.sort()).toEqual([ent1.$$id, ent2.$$id, ent3.$$id].sort())
  })

  test('dim.count()', async () => {
    const sng = await sp.createSingularity()
    const dim = await sng.assignDimension$('test')

    await dim.createEntity({ t: false })
    await dim.createEntity({ t: true })
    await dim.createEntity({ x: 1 })

    expect(dim.count()).resolves.toBe(3)
    expect(dim.count('x')).resolves.toBe(1)
    return expect(dim.count('t')).resolves.toBe(1)
  })
})
