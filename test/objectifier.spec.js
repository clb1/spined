import Redis from 'ioredis'
import { makeId } from '../src/helpers'

import Objectifier from '../src/objectifier'

const redis = new Redis(process.env.REDIS_PORT)

const objectifier = Objectifier({ redis })

afterAll(() => redis.quit())

const $$idA = makeId()
const $$idB = makeId()

const dataA = { id: $$idA, attributes: { value: true }, traits: ['X'] }
const dataB = { id: $$idB, attributes: { other: false }, traits: ['Y'] }

describe('store an Entity in the objectifier', () => {
  let rawEntityA
  let rawEntityB

  test('should return a raw entity', async () => {
    rawEntityA = await objectifier.storeEntity(dataA)
    rawEntityB = await objectifier.storeEntity(dataB)

    expect(rawEntityA).not.toBe(dataA)
    expect(rawEntityB).not.toBe(dataB)

    expect(rawEntityA).toHaveProperty('attributes')
    expect(rawEntityA.attributes).toEqual(dataA.attributes)

    expect(rawEntityA).toHaveProperty('traits')
    expect(rawEntityA.traits).toEqual(dataA.traits)

    expect(rawEntityA).toHaveProperty('createdAt')
    expect(new Date().getTime() - rawEntityA.createdAt).toBeLessThan(1000)

    expect(rawEntityA).toHaveProperty('updatedAt')
    expect(new Date().getTime() - rawEntityA.updatedAt).toBeLessThan(1000)
  })

  test('retrieve on same $$id, should return the same object', async () => {
    const RA1 = await objectifier.retrieveEntity($$idA)

    const RB1 = await objectifier.retrieveEntity($$idB)

    const RA2 = await objectifier.retrieveEntity($$idA)

    expect(RA1).not.toBe(dataA)
    expect(RA1).not.toBe(RB1)
    expect(RA1).toBe(rawEntityA)

    expect(RA2).not.toBe(dataA)
    expect(RA2).toBe(rawEntityA)
    expect(RA2).toBe(RA1)
  })
})
