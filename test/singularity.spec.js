import Redis from 'ioredis'
import Spined from '../src/'
import { expectedId, expectedISO8601, wait } from './utils'

const redis = new Redis(process.env.REDIS_PORT)

const spined = Spined({ redis })

afterAll(() => redis.quit())

describe('create a singularity', () => {
  const tests = [
    { label: 'no args', data: undefined },
    { label: 'no attribute', data: {} },
    { label: 'one attribute', data: { a: true } },
    { label: 'many attributes', data: { a: false, b: 'string', c: 10 } },
    {
      label: 'depth > 1',
      data: { a: false, b: ['x', 3, 4], v: { b: 'string', c: 10 } }
    }
  ]

  for (const t of tests) {
    test(t.label, async () => {
      const S = await spined.createSingularity(t.data)
      const id = S.$$id
      const created = S.$$createdAt

      expect(id).toEqual(expectedId)
      expect(created).toEqual(expectedISO8601)
      expect(S.$$createdAt).toBe(S.$$updatedAt)
      expect(S.$$json).toStrictEqual(t.data || {})
      expect(S.$$traits).toStrictEqual([
        'SINGULARITY',
        'OMNISCIENT',
        'UNBOUNDED'
      ])

      await wait(5)

      const D = await S.duplicate$()
      expect(D.$$id).not.toBe(id)
      expect(D.$$createdAt).not.toBe(created)
      expect(D.$$updatedAt).toEqual(expectedISO8601)
      expect(D.$$createdAt).toBe(D.$$updatedAt)
      expect(D.$$json).toStrictEqual(t.data || {})
      expect(D.$$traits).toStrictEqual([])

      await S.update$({ u: true })
      expect(S.$$json).toStrictEqual({ ...(t.data || {}), u: true })
      expect(S.$$id).toBe(id)
      expect(S.$$createdAt).toBe(created)
      expect(S.$$updatedAt).toEqual(expectedISO8601)
      expect(S.$$updatedAt).not.toBe(created)
      expect(S.u).toBe(true)
      expect(D.u).toBe(undefined)

      await S.delete$()
      expect(() => {
        if (S.u) return true
      }).toThrow('[spined] Entity has been deleted')
      expect(() => {
        S.assignDimension$('x')
      }).toThrow('[spined] Entity has been deleted')
      expect(() => {
        S.delete$()
      }).toThrow('[spined] Entity has been deleted')
      expect(S.$$id).toBe(id)
      expect(S.$$deletedAt).toEqual(expectedISO8601)
      expect(S.$$createdAt).toEqual(created)
      expect(S.$$traits).toStrictEqual([
        'SINGULARITY',
        'OMNISCIENT',
        'UNBOUNDED'
      ])
    })
  }
})
