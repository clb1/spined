# spined

> A JSON semantic network data store on Redis

Spined is an _experimental_ data store to create and manipulate
efficiently persistent data, organized as a "semantic network", a
flexible and high level paradigm.

Spined is extremely opinionated and focus on storing JSON type
documents, using JSONSchema to enforce structure and JSONata as
indexes and queries descriptor.

Spined is written in Node.js and bindings are only available for
Node.js for now.

The low level storage implementation is Redis and Spined generally
preserve the high-performance of Redis and in some cases provide even
faster performance by using an "in memory" application cache.

Though direct Redis API access is always an option, Spined provide a
graph-type abstract and high level organisation of your data for
easier and more human-understandable queries and mutation.

[Documentation](https://spined.io/).
