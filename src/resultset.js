import Entity from './entity'

import { compiled, getExtend, getIndexParams, makeId } from './helpers'

import { S, ENTITY, RESULTSET } from './symbols'

async function * loop (ctx, ids, format = e => e) {
  for (const id of await ids) {
    const entO = await ctx.objectifier.retrieveEntity(
      id,
      getExtend(ctx.$ref, ctx.binding.name, 'options')
    )
    yield format(await Entity(ctx, entO), entO)
  }
}

const prepareConnectionResult = (ctx, ids, scores, card, rank) =>
  Object.freeze({
    edges: loop(ctx, ids, (entity, entO) => ({
      entity,
      cursor: scores[entO.id]
    })),
    fields: {
      totalCount: card,
      pageInfo: {
        hasPreviousPage: rank - ids.length > 0,
        hasNextPage: rank < card,
        startRank: rank - ids.length + 1,
        endRank: rank
      }
    }
  })

const unflatten = a => {
  const ids = []
  const scores = {}
  while (a.length > 0) {
    const entry = a.splice(0, 2)
    ids.push(entry[0])
    scores[entry[0]] = Buffer.from(JSON.stringify(entry)).toString('base64')
  }
  return { ids, scores }
}

const connectionRange = async (
  ctx,
  orderedFiltered,
  { desc, afterRank, first, after, last, before }
) => {
  console.log('HERE', orderedFiltered)

  const card = await ctx.$ref.spined.redis.zcard(orderedFiltered)

  if (!card) return prepareConnectionResult(ctx, [], {}, card, 0)

  if (first) {
    if (afterRank) {
      if (afterRank-- >= card)
        throw new Error('[spined] connection: afterRank must be less than card')
      const pos = desc
        ? await ctx.$ref.spined.redis.zrevrange(
            orderedFiltered,
            afterRank,
            afterRank,
            'WITHSCORES'
          )
        : await ctx.$ref.spined.redis.zrange(
            orderedFiltered,
            afterRank,
            afterRank,
            'WITHSCORES'
          )
      after = pos[1]
    }

    const { ids, scores } = unflatten(
      desc
        ? await ctx.$ref.spined.redis.zrevrangebyscore(
            orderedFiltered,
            `(${after || '+inf'}`,
            '-inf',
            'WITHSCORES',
            'LIMIT',
            0,
            first
          )
        : await ctx.$ref.spined.redis.zrangebyscore(
            orderedFiltered,
            `(${after || '-inf'}`,
            '+inf',
            'WITHSCORES',
            'LIMIT',
            0,
            first
          )
    )

    if (!ids.length) throw new Error('no record found')

    const rank =
      1 +
      (desc
        ? await ctx.$ref.spined.redis.zrevrank(orderedFiltered, ids.slice(-1))
        : await ctx.$ref.spined.redis.zrank(orderedFiltered, ids.slice(-1)))

    return prepareConnectionResult(ctx, ids, scores, card, rank)
  }

  if (last) {
    const { ids, scores } = unflatten(
      desc
        ? await ctx.$ref.spined.redis.zrangebyscore(
            orderedFiltered,
            `(${before || '-inf'}`,
            '+inf',
            'WITHSCORES',
            'LIMIT',
            0,
            last
          )
        : await ctx.$ref.spined.redis.zrevrangebyscore(
            orderedFiltered,
            `(${before || '+inf'}`,
            '-inf',
            'WITHSCORES',
            'LIMIT',
            0,
            last
          )
    )

    if (!ids.length) throw new Error('no record found')

    const rank =
      1 +
      (desc
        ? await ctx.$ref.spined.redis.zrevrank(orderedFiltered, ids.slice(0, 1))
        : await ctx.$ref.spined.redis.zrank(orderedFiltered, ids.slice(0, 1)))

    return prepareConnectionResult(ctx, ids.reverse(), scores, card, rank)
  }
}

const filterRun = jsonata => async (ctx, ids) => {
  const tests = await Promise.all(
    ids.map(async id => {
      const entO = await ctx.objectifier.retrieveEntity(
        id,
        getExtend(ctx.$ref, ctx.binding.name, 'options')
      )
      // TODO handle error gracefully
      return jsonata.evaluate(entO.attributes)
    })
  )
  return ids.filter((id, i) => tests[i])
}

const execute = async (ctx, stack) => {
  let ids = []
  for await (const run of stack) {
    ids = await run(ctx, ids)
    // console.log('execute ->', ids)
  }
  return ids
}

const getSortedSet = async (ctx, ids, ordered) => {
  // TODO optimize shortcut if ids list is same as ordered
  const tmpId = makeId()
  // limit on sadd params is 2,147,483,647.
  // TODO benchmark for extreme case
  await ctx.$ref.spined.redis.sadd(`s.${tmpId}`, ...ids)
  await ctx.$ref.spined.redis.zinterstore(
    `z.${tmpId}`,
    2,
    ordered,
    `s.${tmpId}`
  )
  // ctx.$ref.spined.redis.del s.
  // await ctx.$ref.spined.redis.ttl z.
  return `z.${tmpId}`
}

const getFinalIds = async (ctx, stack, ordered) => {
  let ids = await execute(ctx, stack)
  if (ordered) {
    ids = await ctx.$ref.spined.redis.zrange(
      await getSortedSet(ctx, ids, ordered),
      0,
      -1
    )
  }
  return ids
}

/*  ****************  **************** ****************  **************** */

const Resultset = ({ pristine = false, ...ctx }, stack = [], ordered) => {
  // TODO default value for ordered = dim set

  const $resultset = {
    [S]: true,
    [RESULTSET]: true,
    filter: query => {
      // TODO handle bad JSONata query
      const j = compiled(query)
      return Resultset(ctx, [...stack, filterRun(j)], ordered)
    },
    where: (unique, value) => {
      const run = async (ctx, ids) => {
        // a) get index
        // intersect with ids
        throw new Error(`[spined] where not implemented`)
      }
      // if stack empty alias to index.all
      return Resultset(ctx, [...stack, run], ordered)
    },
    order: cursor => {
      // move indexes out of ctx
      const { hash, canonic, type } = getIndexParams(cursor)
      if (type !== 'C:' || !ctx.indexes.has(canonic))
        throw new Error(`[spined] unknown cursor ${canonic}`)
      const h = hash.update(canonic).digest('base64')
      return Resultset({ pristine, ...ctx }, stack, `${ctx.binding.id}.${h}`)
    },
    has: async any => {
      if (any[ENTITY]) any = any.$$id
      // TODO test id format
      return (await getFinalIds(ctx, stack, ordered)).includes(any)
    }
  }

  $resultset.connection = async (args = { first: 10 }) => {
    if (!ordered)
      throw new Error(`[spined] unordered rs connection not implemented`)
    if (pristine) return connectionRange(ctx, ordered, args)

    throw new Error(`[spined] filtered connnection not implemented`)
    // TODO create addhoc temporary zset if number elements < X entities
  }

  $resultset.ids = async query => {
    if (query) return $resultset.filter(query).ids()
    return getFinalIds(ctx, stack, ordered)
  }

  $resultset.count = async query => {
    if (query) return $resultset.filter(query).count()
    // optimisation with zcard if no stack ?
    return (await getFinalIds(ctx, stack)).length
  }

  $resultset.iterate = cursor => {
    if (cursor) return $resultset.order(cursor).iterate()
    return loop(ctx, getFinalIds(ctx, stack, ordered))
  }

  $resultset.first = async query => {
    if (query) return $resultset.filter(query).first()
    const ids = await getFinalIds(ctx, stack, ordered)
    if (ids.length) {
      const entO = await ctx.objectifier.retrieveEntity(
        ids[0],
        getExtend(ctx.$ref, ctx.binding.name, 'options')
      )
      return Entity(ctx, entO)
    }
  }

  return Object.freeze($resultset)
}

export default Resultset

// RS = name, bounded dimension and index, cachettl, ids,
// RS->pager(<cursor index>)(args)        => also RS!
// RS->connection(<cursor index>)(args)   => also RS!

// can make RS semi persistent
// RS->persist(name, ttl)

// RS->filter(xxx)->filter(xx)->union( RS )

// ent.index('xx').union('C:email', 'xxx@test.com').intersect('xxx').except('').filter(

// if different bounds, bound breaks and oredered is not possible
// bycity.select('Paris').union.(bycity.select('London'))
// rs1 = bycity.select('Paris').union('London')    // syntax sugar
// rs2 = byname.select('Jean').intersect(rs1)
// rs3 = byname.select('Jean').except(rs1)

// RS should not be realized before end of chain
