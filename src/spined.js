import fs from 'fs'

import Objectifier from './objectifier'

import { makeId, isId } from './helpers'

import {
  S,
  ENTITY,
  UNIVERSE,
  REFERENCE,
  DIMENSION,
  RESULTSET,
  INDEX
} from './symbols'

import ReferenceFrame from './refframe'
import Entity from './entity'

const Spined = ({ redis, extend = {}, extendFrom, options = {} }) => {
  const objectifier = Objectifier({ redis })
  const schemaKeywords = new Set([])

  const $spined = {
    get redis () {
      return redis
    },
    get extend () {
      return extend
    },
    get options () {
      return options
    },
    get schemaKeywords () {
      return schemaKeywords
    },
    isSpined: x => x && !!x[S],
    isEntity: x => x && !!x[ENTITY],
    isDimension: x => x && !!x[DIMENSION],
    isResultset: x => x && !!x[RESULTSET],
    isIndex: x => x && !!x[INDEX],
    isReferenceFrame: x => x && !!x[REFERENCE],
    get$$id: x => (x[ENTITY] ? x.$$id : x),
    addSchemaKeyword: (...args) => schemaKeywords.add(args) // XXX TODO hash ? or Array ?
  }
  Reflect.defineProperty($spined, S, { enumerable: false, value: true })
  Reflect.defineProperty($spined, UNIVERSE, { enumerable: true, value: true })
  Reflect.defineProperty($spined, 'redis', { enumerable: false })
  Reflect.defineProperty($spined, 'options', { enumerable: true })

  $spined.dump = (any, { depth = 1 } = {}) => {
    if (!any || !any[S]) {
      throw new Error('dump argument should be an Entity, Dimension, Index')
    }
    console.dir(any, { getters: true, depth })
  }

  // should be move in plugin id
  $spined.id = any => {
    if (any == null) throw new Error('Spined.id attempt on null value')
    if (any[ENTITY]) return any.$$id
    if (isId(any)) return any
    throw new Error(`${any} is neither Entity or id`)
  }

  const loadExtendFrom = async () => {
    const files = fs.readdirSync(extendFrom)
    await Promise.all(
      files.map(async f => {
        if (
          /^[a-zA-Z]+\.js$/.test(f) &&
          fs.lstatSync(`${extendFrom}${f}`).isFile()
        ) {
          const module = await import(`${extendFrom}${f}`)
          // Better filtering (use symbol ?)
          if (module.default && module.default.schema) {
            const dimName = f.split('.')[0]
            // console.log(`Loading '${dimName}' dimension extend from file ${extendFrom}${f}`);
            extend[dimName] = module.default
          }
        }
      })
    )
    extendFrom = null
  }

  $spined.createSingularity = async (attributes = {}) => {
    if (extendFrom) await loadExtendFrom()
    const o = await objectifier.storeEntity({
      id: makeId(),
      attributes,
      traits: ['SINGULARITY', 'OMNISCIENT', 'UNBOUNDED']
    })
    redis.sadd(`[spined].singularity`, o.id)
    return Entity(
      { objectifier, $ref: ReferenceFrame($spined, objectifier, o) },
      o
    )
  }

  $spined.referenceFrame = async id => {
    if (extendFrom) await loadExtendFrom()
    const o = await objectifier.retrieveEntity(id)
    if (!o.traits || !o.traits.includes('SINGULARITY'))
      throw new Error(
        '[spined] not a singularity, cannot retreive reference frame'
      )
    return ReferenceFrame($spined, objectifier, o)
  }

  $spined.defineReferenceFrame = async (...entities) => {
    let test = false
    let aliases = []
    if (
      typeof entities[0] === 'object' &&
      entities[0] !== null &&
      !entities[0][ENTITY]
    ) {
      aliases = Object.keys(entities[0])
      entities = Object.values(entities[0])
    }
    const objects = []
    for (const e of entities) {
      if (!e[ENTITY])
        throw new Error('[spined] only entities can define reference frame')
      test = test || e.$$ref.isOmniscient
      const o = await objectifier.retrieveEntity(e.$$id)
      objects.push(o)
    }
    if (!test)
      throw new Error(
        '[spined] need omniscient reference frame to define a new one'
      )
    return ReferenceFrame($spined, objectifier, objects, aliases)
  }

  return Object.freeze($spined)
}

export default Spined
