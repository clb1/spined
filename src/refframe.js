import { S, REFERENCE, EO } from './symbols'

import Entity from './entity'

const ReferenceFrame = ($spined, objectifier, sources, aliases = []) => {
  if (!Array.isArray(sources) && sources[EO]) {
    sources = [sources]
  } else {
    if (!Array.isArray(sources))
      throw new Error('[spined] fatal error creating reference frame')
    sources.forEach(o => {
      if (!o[EO])
        throw new Error('[spined] fatal error creating reference frame')
    })
  }

  while (sources.length > aliases.length) aliases.push(aliases.length)

  aliases = aliases.reduce(
    (acc, curr, index) => Object.assign(acc, { [curr]: index }),
    {}
  )

  const $ref = {
    [S]: true,
    [REFERENCE]: true,
    get spined () {
      return $spined
    },
    get aliases () {
      return ['default', ...Object.keys(aliases)]
    },
    get sources () {
      return Object.keys(aliases).reduce(
        (acc, curr) =>
          Object.assign(acc, { [curr]: sources[aliases[curr]].id }),
        {}
      )
    }
  }

  Object.keys(aliases).map(alias => {
    Reflect.defineProperty($ref, alias, {
      enumerable: true, // TODO change ?
      // TODO remove binding from $ref
      get () {
        return Entity({ objectifier, $ref }, sources[aliases[alias]])
      }
    })
  })

  Reflect.defineProperty($ref, 'default', {
    enumerable: true,
    get () {
      return $ref[Object.keys(aliases)[0]]
    }
  })

  Reflect.defineProperty($ref, 'isOmniscient', {
    enumerable: false,
    get () {
      for (const s of sources) if (s.traits.includes('OMNISCIENT')) return true
      return false
    }
  })

  Reflect.defineProperty($ref, 'isUnbounded', {
    enumerable: false,
    get () {
      for (const s of sources) if (s.traits.includes('UNBOUNDED')) return true
      return false
    }
  })

  return Object.freeze($ref)
}

export default ReferenceFrame
