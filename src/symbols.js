export const S = Symbol('Spined')

// TODO replace with UNIVERSE
export const UNIVERSE = Symbol('Spined.Universe')
export const REFERENCE = Symbol('Spined.Reference')

// internal stored Entity & Dimension object
export const EO = Symbol('Spined.EO')
export const DO = Symbol('Spined.DO')

export const ENTITY = Symbol('Spined.Entity')
export const TRAITS = Symbol('Spined.Traits')
export const OBJECT = Symbol('Spined.Object')

export const DIMENSION = Symbol('Spined.Dimension')
export const RESULTSET = Symbol('Spined.Resultset')
export const INDEX = Symbol('Spined.Index')
export const INDEXTYPE = Symbol('Spined.IndexType')

export const NORMALIZE = Symbol('Spined.MN')
