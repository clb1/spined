import { getExtend, makeId } from './helpers'

import Entity from './entity'
import Dimension from './dimension'

export const PROXY = Symbol('Spined.Proxy')

export const $handler = {
  deleteProperty (target, prop) {
    // to intercept property deletion
    throw new Error('[spined] You cannot directly delete Entity property')
  },
  getOwnPropertyDescriptor: function (target, prop) {
    // called for every property
    // console.log('getOwnPropertyDescriptor', prop );
    return Reflect.getOwnPropertyDescriptor(target, prop)
  },
  enumerate: function (target, prop) {
    throw new Error("[spined] Please don't use enumerate on Entity object.")
  },
  ownKeys: function (target, prop) {
    console.log(`ownKeys ${prop}`, [...Reflect.ownKeys(target)])
    return [...Reflect.ownKeys(target)]
  },
  set: function (target, prop, value, receiver) {
    if (/^\$\$/.test(prop)) {
      throw new Error(`Spinned: Entity ${prop} is not writable.`)
    }
    return target.update$({ [prop]: value })
  },
  get: function (target, prop) {
    if (prop === PROXY) return true

    // TODO: how to get 'then' attribut not hijacked by Promise def ?
    if (prop === 'then') return null

    // TODO hack all common xx$ to subhandler...

    if (Reflect.has(target, '$$deletedAt') && !/^\$\$.*/.test(prop))
      throw new Error('[spined] Entity has been deleted')

    if (Reflect.has(target, prop)) return Reflect.get(target, prop)

    return target.get$(prop)
  }
}

export const update$ = (ctx, o, $proxy) => async attributes => {
  if (o.traits.includes('IMMUTABLE'))
    throw new Error(`[spined] cannot update an IMMUTABLE Entity`)
  await ctx.objectifier.storeEntity({
    ...o,
    attributes: { ...o.attributes, ...attributes },
    updatedAt: undefined
  })
  return $proxy
}

export const duplicate$ = (ctx, o, $proxy) => async () => {
  if (o.traits.includes('INVISIBLE'))
    throw new Error(`[spined] cannot duplicate an INVISIBLE Entity`)
  const dupo = await ctx.objectifier.storeEntity({
    id: makeId(),
    attributes: { ...o.attributes },
    traits: []
  })
  return Entity(ctx, dupo)
}

export const delete$ = (ctx, o, $entity, $proxy) => async () => {
  if (o.traits.includes('PERPETUAL'))
    throw new Error(`[spined] cannot delete a PERPETUAL Entity`)
  let ok = true
  const beforeDelete = getExtend(
    ctx.$ref,
    ctx.binding?.name,
    'triggers',
    'beforeDelete'
  )
  if (beforeDelete) {
    ok = await beforeDelete(ctx.$ref, $entity)
  }
  if (ok) {
    // XXX problem find all dimensions & indexes ...
    if (ctx.binding) {
      const x = await Dimension(ctx, ctx.binding) // we don't need extend here ??
      await x.removeEntity($entity) // check return true
    }
    if (await ctx.objectifier.deleteEntity(o.id)) {
      Reflect.defineProperty($entity, '$$deletedAt', {
        enumerable: true,
        get () {
          return new Date(o.deletedAt).toJSON()
        }
      })
      return true
    }
  }
}
