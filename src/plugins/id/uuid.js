import { v4 as uuidv4 } from 'uuid'

export const makeId = () => uuidv4()

const uuidV4Regex = /^[A-F\d]{8}-[A-F\d]{4}-4[A-F\d]{3}-[89AB][A-F\d]{3}-[A-F\d]{12}$/i

export const isId = id => uuidV4Regex.test(id)
