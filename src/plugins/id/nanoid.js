import { nanoid } from 'nanoid'

export const makeId = () => nanoid()

const nanoidRegex = /^[A-Za-z0-9_-]{21}$/

export const isId = id => nanoidRegex.test(id)
