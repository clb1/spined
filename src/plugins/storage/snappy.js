import snappy from 'snappy'

export const serialize = o => snappy.compressSync(JSON.stringify(o))
export const unserialize = async (id, redis) => {
  // TODO non compressed case depending on options
  const b = await redis.getBuffer(id)
  if (!b) throw new Error(`spined: ${id} doesn't exist in Redis`)
  JSON.parse(snappy.uncompressSync(b, { asBuffer: false }))
}
