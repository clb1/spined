
@{%
const moo = require('moo')

const lexer = moo.compile({
  nl: { match: /\n/, lineBreaks: true },
  space:  /[\ \t]+/,
  keyword: ['select', 'from', 'rf'],
  id: /[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-4[A-Fa-f\d]{3}-[89ABab][A-Fa-f\d]{3}-[A-Fa-f\d]{12}/,
  comment: /\/\/.*?$/,
  number:  /0|[1-9][0-9]*/,
  string:  /"(?:\\["\\]|[^\n"\\])*"/,
  lparen:  '(',
  rparen:  ')',
  lbrace:  '{',
  rbrace:  '}',
  times:  /\*/,
  identifier: /[A-Za-z][A-Za-z1-9]*/,
  number:  /0|[1-9][0-9]*/,
})
%}

# Pass your lexer object using the @lexer option:
@lexer lexer

# commands -> (command commands) | null {% function(d) { return d[0] } %}

command -> (referenceFrame | select | list ) %nl {% function(d) { return d[0] } %}

referenceFrame -> ("referenceFrame" | "rf") ( _ %id | null ) {%
       d => ({ method: 'referenceFrame', entity: d[3] })
%}

list -> "list" _ "dimensions" {%
       d => ({ method: 'listDimension$', property: 'listDimension$' })
%}

select -> "select" _ %times _ "from" _ %identifier {%
       d => ({ method: 'select', entity: d[6] })
%}

_ -> %space {% function(d) { return null } %}
