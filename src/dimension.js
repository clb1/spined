import Ajv from 'ajv'

import Entity from './entity'
import Resultset from './resultset'
import Index from './indexes'

import {
  getExtend,
  setEntityIndex,
  unsetEntityIndex,
  getIndexParams,
  makeId
} from './helpers'

import { S, ENTITY, DIMENSION } from './symbols'

/*  ****************  **************** ****************  **************** */

const Dimension = async (ctx, o) => {
  const indexes = new Set(
    o.meta.includes('INDEXED')
      ? await ctx.$ref.spined.redis.smembers(`${o.id}.I`)
      : []
  )

  let validate

  const prepareValidation = () => {
    if (o.schema) {
      const ajv = new Ajv()
      ctx.$ref.spined.schemaKeywords.forEach(args => ajv.addKeyword(...args))
      validate = ajv.compile(o.schema)
    }
  }

  prepareValidation()

  const link = async entO => {
    await ctx.$ref.spined.redis.sadd(`${o.id}.E`, entO.id)
    await ctx.$ref.spined.redis.sadd(`${entO.id}.M`, o.id)
    await Promise.all(
      Array.from(indexes).map(identifier =>
        setEntityIndex(ctx, o, entO, identifier)
      )
    )
    return Entity({ ...ctx, binding: o }, entO)
  }

  const unlink = async entO => {
    await ctx.$ref.spined.redis.srem(`${o.id}.E`, entO.id)
    await ctx.$ref.spined.redis.srem(`${entO.id}.M`, o.id)
    await Promise.all(
      Array.from(indexes).map(identifier =>
        unsetEntityIndex(ctx, o, entO, identifier)
      )
    )
    return true
  }

  const runValidation = attributes => {
    const result = validate(attributes)
    if (!result) {
      const Errors = validate.errors.map(err => {
        // TODO better error format
        if (err.dataPath) {
          return `${err.dataPath.substr(1)} ${err.message}`
        }
        return err
      })
      throw new Error(JSON.stringify({ attributes, Errors }))
    }
    return true
  }

  const $dimension = {
    [S]: true,
    [DIMENSION]: true,
    get id () {
      return o.id
    },
    get name () {
      return o.name
    },
    get schema () {
      return o.schema
    },
    get meta () {
      return o.meta
    },
    get indexes () {
      return indexes
    },
    get extend () {
      return ctx.extend[o.name]
    },
    // addMethod: (name, fct) => connect to id ... at db level !
    createEntity: async (
      attributes = {},
      traits = [],
      createdAt,
      updatedAt,
      id
    ) => {
      if (
        !ctx.$ref.spined.options.overloadTimestamp &&
        createdAt !== undefined
      ) {
        throw new Error(
          '[spined] overloadTimestamp is false, cannot set custom createdAt'
        )
      }
      if (
        !ctx.$ref.spined.options.overloadTimestamp &&
        updatedAt !== undefined
      ) {
        throw new Error(
          '[spined] overloadTimestamp is false, cannot set custom updatedAt'
        )
      }
      if (!ctx.$ref.spined.options.overloadId && id) {
        throw new Error('[spined] overloadId is false, cannot set custom id')
      }

      const triggers = getExtend(ctx.$ref, o.name, 'triggers')

      if (triggers.beforeCreate) {
        attributes = await triggers.beforeCreate(ctx.$ref, attributes)
      }

      if (o.schema) runValidation(attributes)

      const entO = await ctx.objectifier.storeEntity(
        {
          attributes,
          traits,
          createdAt,
          updatedAt,
          id: makeId()
        },
        getExtend(ctx.$ref, o.name, 'options')
      )
      return link(entO)
    },
    addEntity: async E => {
      if (!E[ENTITY])
        throw new Error(`[spined] can only add Entity to dimension ${o.id}`)
      if (o.schema) runValidation(E.attributes)
      const entO = await ctx.objectifier.retrieveEntity(
        E.$$id,
        getExtend(ctx.$ref, o.name, 'options')
      )
      return link(entO)
    },
    removeEntity: async E => {
      if (!E[ENTITY])
        throw new Error(`[spined] can only remove Entity to dimension ${o.id}`)
      // TODO id is member
      const entO = await ctx.objectifier.retrieveEntity(
        E.$$id,
        getExtend(ctx.$ref, o.name, 'options')
      )
      return unlink(entO)
    },
    addIndex: async identifier => {
      throw new Error('[spined] not implemented')
      // ctx.$ref.spined.redis.sadd(`${o.id}.I`, identifier)
    },
    deleteIndex: async identifier => {
      throw new Error('[spined] not implemented')
    },
    index: identifier => {
      const { hash, canonic, type } = getIndexParams(identifier)
      if (!indexes.has(canonic)) {
        throw new Error(`[spined] dimension has no index '${canonic}'`)
      }
      return Index({ ...ctx, binding: o, indexes }, { hash, canonic, type })
    },
    setSchema: async schema => {
      // TODO check meta schema...
      // throw new Error(`not implemented`);
      await ctx.objectifier.storeDimension({
        ...o,
        schema
      })
      prepareValidation()
    },
    retrieve: async id => {
      if (await ctx.$ref.spined.redis.sismember(`${o.id}.E`, id)) {
        return Entity(
          { ...ctx, binding: o },
          await ctx.objectifier.retrieveEntity(
            id,
            getExtend(ctx.$ref, o.name, 'options')
          )
        )
      } else {
        throw new Error(`[spined] can not retrieve this id`)
      }
    },
    has: async any => {
      if (any[ENTITY]) any = any.$$id
      // TODO test id format
      return !!(await ctx.$ref.spined.redis.sismember(`${o.id}.E`, any))
    },
    all: () => {
      const run = ctx => ctx.$ref.spined.redis.smembers(`${o.id}.E`)
      return Resultset({ ...ctx, binding: o, pristine: true, indexes }, [run])
    }
    // populate: async iterator => {
    //   // TODO move addEntity in common function
    //   for await (const E of await iterator) {
    //     const entO = await ctx.objectifier.retrieveEntity(E.$$id, getExtend(ctx.$ref, o.name, 'options'))
    //     await link(entO)
    //   }
    // }
  }

  /* syntaxic sugar methods */

  $dimension.order = $dimension.all().order
  $dimension.iterate = $dimension.all().iterate

  $dimension.filter = $dimension.all().filter

  $dimension.first = $dimension.all().first

  $dimension.ids = $dimension.all().ids

  $dimension.count = async query => {
    if (query) return $dimension.filter(query).count()
    // small shortcut if no query
    return ctx.$ref.spined.redis.scard(`${o.id}.E`)
  }

  return Object.freeze($dimension)
}

export default Dimension
