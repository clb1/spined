import { getExtend, getDimension, getIndexParams, makeId } from './helpers'

import { $handler, update$, delete$, duplicate$ } from './handler'

import { S, ENTITY } from './symbols'

import Dimension from './dimension'

// ctx is not directly accessible
const Entity = (ctx, o) => {
  const getIndex = async ({ dimName, constraint }) => {
    const dimension = await getDimension(ctx, o)(dimName)
    return dimension.index(constraint)
  }

  // XXX only use defineProperty ?
  const $entity = {
    get $$json () {
      return JSON.parse(JSON.stringify(o.attributes))
    },
    get $$createdAt () {
      return new Date(o.createdAt).toJSON()
    },
    get $$updatedAt () {
      return new Date(o.updatedAt).toJSON()
    },
    get $$json$$ () {
      return JSON.parse(
        JSON.stringify({
          $$id: o.id,
          $$createdAt: new Date(o.createdAt).toJSON(),
          $$updatedAt: new Date(o.updatedAt).toJSON(),
          ...o.attributes
        })
      )
    },
    get $$traits () {
      return o.traits || []
    }
  }

  Reflect.defineProperty($entity, S, { value: true })
  Reflect.defineProperty($entity, ENTITY, { enumerable: false, value: true })
  Reflect.defineProperty($entity, '$$id', {
    enumerable: true,
    get () {
      return o.id
    }
  })
  Reflect.defineProperty($entity, '$$json', { enumerable: false })
  Reflect.defineProperty($entity, '$$json$$', { enumerable: false })
  Reflect.defineProperty($entity, '$$traits', { enumerable: false })
  Reflect.defineProperty($entity, '$$ref', {
    enumerable: false,
    get () {
      return ctx.$ref
    }
  })

  Reflect.defineProperty($entity, '$$dimensions', {
    enumerable: false,
    get () {
      return new Promise(resolve => {
        resolve(ctx.$ref.spined.redis.hkeys(`${o.id}.D`))
      })
    }
  })

  Reflect.defineProperty($entity, 'hasDimension$', {
    enumerable: false,
    value: async name => {
      const all = await ctx.$ref.spined.redis.hkeys(`${o.id}.D`)
      return all.includes(name)
    }
  })

  const $proxy = new Proxy($entity, $handler)

  // TODO rather define only at proxy level ??
  Reflect.defineProperty($entity, 'dump$', {
    enumerable: false,
    value: () => {
      console.dir($entity, { getters: true, depth: 2 })
    }
  })

  Reflect.defineProperty($entity, 'hasTrait$', {
    enumerable: false,
    value: t => o.traits.includes(t)
  })

  Reflect.defineProperty($entity, 'addTrait$', {
    enumerable: false,
    value: async t => {
      if (t === 'SINGULARITY')
        throw new Error(`[spined] SINGULARITY is a reserved trait`)
      if (t === 'OMNISCIENT' && !ctx.$ref.isOmniscient)
        throw new Error(`[spined] current Reference Frame not omniscient`)
      if (!ctx.$ref.isUnbounded)
        throw new Error(`[spined] current Reference Frame not unbounded`)
      await ctx.objectifier.storeEntity({ ...o, traits: [...o.traits, t] })
      return true
    }
  })

  Reflect.defineProperty($entity, 'delTrait$', {
    enumerable: false,
    value: async t => {
      if (t === 'SINGULARITY')
        throw new Error(`[spined] SINGULARITY is a reserved trait`)
      if (t === 'OMNISCIENT' && !ctx.$ref.isOmniscient)
        throw new Error(`[spined] current Reference Frame not omniscient`)
      if (!ctx.$ref.isUnbounded)
        throw new Error(`[spined] current Reference Frame not unbounded`)
      await ctx.objectifier.storeEntity({
        ...o,
        traits: o.traits.filter(x => x !== t)
      })
      return true
    }
  })

  Reflect.defineProperty($entity, 'index$', {
    enumerable: false,
    value: obj => getIndex(obj)
  })

  // TODO $$dimension => look in all dimensions of the reference frames

  Reflect.defineProperty($entity, 'dimension$', {
    enumerable: false,
    value: getDimension(ctx, o)
  })

  Reflect.defineProperty($entity, 'assignDimension$', {
    enumerable: false,
    value: async (name, { schema, indexes = [] } = {}) => {
      const dimNames = new Set(await ctx.$ref.spined.redis.hkeys(`${o.id}.D`))
      if (dimNames.has(name))
        throw new Error(`[spined] ${o.id} already has a dimension '${name}'`)
      const id = makeId()
      const dimO = await ctx.objectifier.storeDimension({
        id,
        name,
        meta: indexes.length ? ['INDEXED'] : [],
        schema,
        entityId: o.id
      })
      await ctx.$ref.spined.redis.hset(`${o.id}.D`, name, id)
      await Promise.all(
        indexes.map(identifier => {
          const { canonic } = getIndexParams(identifier)
          return ctx.$ref.spined.redis.sadd(`${id}.I`, canonic)
        })
      )
      return Dimension(ctx, dimO)
    }
  })

  Reflect.defineProperty($entity, 'revokeDimension$', {
    enumerable: false,
    value: async name => {
      throw new Error(`[spined] revokeDimension not implemented yet`)
    }
  })

  Reflect.defineProperty($entity, 'update$', {
    enumerable: false,
    value: update$(ctx, o, $proxy)
  })

  Reflect.defineProperty($entity, 'duplicate$', {
    enumerable: false,
    value: duplicate$(ctx, o, $proxy)
  })

  Reflect.defineProperty($entity, 'delete$', {
    enumerable: false,
    value: delete$(ctx, o, $entity, $proxy)
  })

  // XXX move to handler ...
  Reflect.defineProperty($entity, 'inc$', {
    enumerable: false,
    value: async name => {
      if (o.traits.includes('IMMUTABLE'))
        throw new Error(`[spined] cannot update an IMMUTABLE Entity`)
      if (!o.attributes[name])
        throw new Error(`[spined] ${o.id} has no attribute ${name}'`)
      await ctx.objectifier.storeEntity({
        ...o,
        attributes: { ...o.attributes, [name]: o.attributes[name] + 1 }
      })
      return o.attributes[name]
    }
  })

  if (ctx.binding && ctx.binding.name) {
    Object.keys(getExtend(ctx.$ref, ctx.binding?.name, 'getters')).forEach(
      name => {
        Reflect.defineProperty($entity, name, {
          enumerable: true,
          get: getExtend(
            ctx.$ref,
            ctx.binding?.name,
            'getters',
            name
          )(ctx.$ref, $proxy)
        })
      }
    )
  }

  Reflect.defineProperty($entity, 'get$', {
    enumerable: false,
    value: name => {
      if (o.traits.includes('INVISIBLE'))
        throw new Error(`[spined] cannot read an INVISIBLE Entity`)
      return typeof o.attributes[name] === 'object'
        ? JSON.parse(JSON.stringify(o.attributes[name]))
        : o.attributes[name]
    }
  })

  if (ctx.binding && ctx.binding.name) {
    Object.keys(getExtend(ctx.$ref, ctx.binding?.name, 'methods')).forEach(
      name => {
        Reflect.defineProperty($entity, name, {
          enumerable: true,
          value: (...args) =>
            getExtend(
              ctx.$ref,
              ctx.binding?.name,
              'methods',
              name
            )(
              ctx.$ref,
              $proxy
            )(...args)
        })
      }
    )
  }

  /* XXX TODO dynamic model to be provided only with special starting option?
  Reflect.defineProperty($entity, 'addMethod$', {
    enumerable: false,
    value: async (name, fct) => {
      Reflect.defineProperty($entity, name, {
        enumerable: false,
        value: (...args) => fct(ctx.$ref, $proxy)(...args)
      })
    }
  })
  */

  return $proxy
}

export default Entity
