// Generated automatically by nearley, version 2.20.1
// http://github.com/Hardmath123/nearley
;(function () {
  function id (x) {
    return x[0]
  }

  const moo = require('moo')

  const lexer = moo.compile({
    nl: { match: /\n/, lineBreaks: true },
    space: /[\ \t]+/,
    keyword: ['select', 'from', 'rf'],
    id: /[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-4[A-Fa-f\d]{3}-[89ABab][A-Fa-f\d]{3}-[A-Fa-f\d]{12}/,
    comment: /\/\/.*?$/,
    number: /0|[1-9][0-9]*/,
    string: /"(?:\\["\\]|[^\n"\\])*"/,
    lparen: '(',
    rparen: ')',
    lbrace: '{',
    rbrace: '}',
    times: /\*/,
    identifier: /[A-Za-z][A-Za-z1-9]*/,
    number: /0|[1-9][0-9]*/
  })
  var grammar = {
    Lexer: lexer,
    ParserRules: [
      { name: 'command$subexpression$1', symbols: ['referenceFrame'] },
      { name: 'command$subexpression$1', symbols: ['select'] },
      { name: 'command$subexpression$1', symbols: ['list'] },
      {
        name: 'command',
        symbols: [
          'command$subexpression$1',
          lexer.has('nl') ? { type: 'nl' } : nl
        ],
        postprocess: function (d) {
          return d[0]
        }
      },
      {
        name: 'referenceFrame$subexpression$1',
        symbols: [{ literal: 'referenceFrame' }]
      },
      { name: 'referenceFrame$subexpression$1', symbols: [{ literal: 'rf' }] },
      {
        name: 'referenceFrame$subexpression$2',
        symbols: ['_', lexer.has('id') ? { type: 'id' } : id]
      },
      { name: 'referenceFrame$subexpression$2', symbols: [] },
      {
        name: 'referenceFrame',
        symbols: [
          'referenceFrame$subexpression$1',
          'referenceFrame$subexpression$2'
        ],
        postprocess: d => ({ method: 'referenceFrame', entity: d[3] })
      },
      {
        name: 'list',
        symbols: [{ literal: 'list' }, '_', { literal: 'dimensions' }],
        postprocess: d => ({
          method: 'listDimension$',
          property: 'listDimension$'
        })
      },
      {
        name: 'select',
        symbols: [
          { literal: 'select' },
          '_',
          lexer.has('times') ? { type: 'times' } : times,
          '_',
          { literal: 'from' },
          '_',
          lexer.has('identifier') ? { type: 'identifier' } : identifier
        ],
        postprocess: d => ({ method: 'select', entity: d[6] })
      },
      {
        name: '_',
        symbols: [lexer.has('space') ? { type: 'space' } : space],
        postprocess: function (d) {
          return null
        }
      }
    ],
    ParserStart: 'command'
  }
  if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
    module.exports = grammar
  } else {
    window.grammar = grammar
  }
})()
