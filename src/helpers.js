import jsonata from 'jsonata'

import Entity from './entity'
import Dimension from './dimension'
import Map from './score-maps'
import { NORMALIZE } from './symbols'

import { makeId, isId } from './plugins/id/nanoid'

export { makeId, isId }

const crypto = require('crypto')

// TODO handle to big queries table ?
const QUERY = {}

export const getExtend = ($ref, dimName, section, entry) => {
  const table =
    $ref && dimName && $ref.spined.extend && $ref.spined.extend[dimName]
      ? $ref.spined.extend[dimName][section] || {}
      : {}
  if (entry) return table[entry]
  else return table
}

// generalized to be useful in rs.js
export const getEntity = (ctx, dimO) => async id =>
  Entity(
    { ...ctx, binding: dimO },
    await ctx.objectifier.retrieveEntity(
      id,
      getExtend(ctx.$ref, dimO.name, 'options')
    )
  )

export const getDimension = (ctx, entO) => async name => {
  const id = await ctx.$ref.spined.redis.hget(`${entO.id}.D`, name)
  if (!id) throw new Error(`[spined] dimension '${name}' doesn't exist`)
  // TODO what if not exist
  const dimO = await ctx.objectifier.retrieveDimension(id)
  return Dimension(ctx, dimO)
}

export const compiled = query => {
  // TODO shorcut simple access
  if (!Object.prototype.hasOwnProperty.call(QUERY, query)) {
    QUERY[query] = jsonata(query)
  }
  return QUERY[query]
}

export const getIndexParams = identifier => {
  // use sub grammar/parser ...
  const hash = crypto.createHash('sha1') // TODO move out
  const type = identifier.slice(0, 2)
  let query = identifier.slice(2)
  query = query.replace(/(^\s+)|(\s+$)/g, '')
  let map = ''
  if (type === 'C:') {
    map = ['$$createdAt', '$$updatedAt'].includes(query) ? 'Num:' : 'AlphaNum:'
    if (/^(AlphaNum|Num):/.test(query)) {
      const all = query.split(':')
      map = all.shift() + ':'
      query = all.join(':')
    }
  }
  // console.log({ canonic: type + map + query, type, query: `*${query}*`, map })
  return { hash, canonic: type + map + query, type, query, map }
}

export const getIndexValue = (query, entO) => {
  const j = compiled(query)
  return j.evaluate(entO.attributes, {
    $createdAt: entO.createdAt,
    $updatedAt: entO.updatedAt,
    $id: entO.id
  })
}

export const getCursorScore = (map = 'AlphaNum:', str = '') => {
  if (map === 'Num:') return parseInt(str)
  const normalized = Map[map][NORMALIZE](str)
  let binary = ''
  // Max int nodejs = 9007199254740992 // IEEE 754 (53 bits)
  // TODO use sign for score ?
  for (let i = 0; binary.length < 53 && i < 53; i++) {
    binary += Map[map][normalized.charAt(i)]
  }
  return parseInt(binary.substring(0, 53), 2)
}

// TODO on update => we need to re-link all dimensions the Entity is members of !
// so we need to tracks entity dimensions ...

// refactor with newvalues/oldvalues as optimisation
// identifier can be any jsonNata query to calculate a score
/// : unique index eg email => hset good, zadd good to sort by lex

export const setEntityIndex = async (ctx, o, entO, identifier, oldO) => {
  const { hash, canonic, type, query, map } = getIndexParams(identifier)

  // SET
  if (type === 'S:') {
    const value = getIndexValue(query, entO)
    const h = hash.update(canonic + value).digest('base64')
    // console.log(`adding SET index ${o.id}.${canonic} = ${value}`);
    return ctx.$ref.spined.redis.sadd(`${o.id}.${h}`, entO.id)
  }

  // UNIQUE
  if (type === 'U:') {
    const value = getIndexValue(query, entO)
    const h = hash.update(canonic).digest('base64')
    if ((await ctx.$ref.spined.redis.hget(`${o.id}.${h}`, value)) != null)
      throw new Error(
        `[spined] orphaning ${entO.id}, doesn't respect ${canonic}`
      )
    // console.log(`adding UNIQUE index ${o.id}.${canonic} = ${value}`, query);
    return ctx.$ref.spined.redis.hset(`${o.id}.${h}`, value, entO.id)
  }

  // CURSOR
  if (type === 'C:') {
    // TODO special type with some bits are reserved too id to get heterogeneous scores!
    const score = getCursorScore(map, getIndexValue(query, entO))
    const h = hash.update(canonic).digest('base64')
    // XXX handle the exception that new record don't have a valid value to calculate score
    // console.log(`adding CURSOR ${getIndexValue(query, entO)}.${canonic} ${map} = ${score}`)
    return ctx.$ref.spined.redis.zadd(`${o.id}.${h}`, score, entO.id)
  }

  throw new Error(`[spined] unrecognized index identifier ${identifier}`)
}

// merge set//unset in separte module per each index type
export const unsetEntityIndex = async (ctx, o, entO, identifier) => {
  const { hash, canonic, type, query } = getIndexParams(identifier)

  if (type === 'S:') {
    const value = getIndexValue(query, entO)
    const h = hash.update(canonic + value).digest('base64')
    return ctx.$ref.spined.redis.srem(`${o.id}.${h}`)
  }

  if (type === 'U:') {
    const value = getIndexValue(query, entO)
    const h = hash.update(canonic).digest('base64')
    return ctx.$ref.spined.redis.hdel(`${o.id}.${h}`, value)
  }

  if (type === 'C:') {
    const h = hash.update(canonic).digest('base64')
    return ctx.$ref.spined.redis.zrem(`${o.id}.${h}`, entO.id)
  }

  throw new Error(`[spined] unrecognized Index identifier ${identifier}`)
}
