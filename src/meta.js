const INDEXED = 0b000001

const META = {
  INDEXED
}

export default META
