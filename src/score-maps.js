import { NORMALIZE } from './symbols'

const generateAlphaNumMap = () => {
  // 8.3 chars max mapped with 6 bits score
  const map = {
    // XXX TODO check edges punctation cases
    [NORMALIZE]: data =>
      data
        .toString()
        .toUpperCase()
        .replace(/[.]/g, ' ')
        .replace(/[^A-Z0-9_ ]/g, '#')
        .padEnd(9, ' '),
    ' ': '000000',
    _: '001011',
    '#': '100110'
  }
  for (let i = 0; i < 10; i++) {
    map['' + i] = (i + 1).toString(2).padStart(6, '0')
  }
  for (let i = 65; i < 91; i++) {
    map[String.fromCharCode(i)] = (i - 53).toString(2).padStart(6, '0')
  }
  return map
}

const Map = Object.freeze({
  'AlphaNum:': generateAlphaNumMap()
})

export default Map
