import { S, INDEX, INDEXTYPE } from './symbols'

import Resultset from './resultset'

import { getEntity } from './helpers'

// rename Set to query ?

const LONGINDEXTYPE = { U: 'Unique', S: 'Set', C: 'Cursor' }

const Index = ({ binding, ...ctx }, { hash, canonic, type }) => {
  const $index = {
    canonic,
    [INDEXTYPE]: LONGINDEXTYPE[type],
    [INDEX]: true,
    [S]: true
  }

  if (type === 'U:') {
    const h = hash.update(canonic).digest('base64')
    const find = async str => {
      const id = await ctx.$ref.spined.redis.hget(`${binding.id}.${h}`, str)
      return id ? getEntity({ binding, ...ctx }, binding)(id) : undefined
    }
    return Object.freeze({
      find,
      findElseThrow: async str => {
        const ent = await find(str)
        if (ent !== undefined) return ent
        throw new Error(
          `${binding.id}-${binding.name}.${canonic} no entity found with ${canonic} = ${str}`
        )
      },
      ...$index
    })
  }

  if (type === 'S:') {
    const all = str => {
      const h = hash.update(canonic + str).digest('base64')
      const run = ctx => ctx.$ref.spined.redis.smembers(`${binding.id}.${h}`)
      return Resultset({ binding, ...ctx }, [run])
    }
    return Object.freeze({
      all,
      ids: str => all(str).ids(),
      count: str => all(str).count(), // TODO explicit to optimze ?
      first: str => all(str).first(),
      iterate: str => all(str).iterate(),
      ...$index
    })
  }

  if (type === 'C:') {
    const h = hash.update(canonic).digest('base64')
    return Object.freeze({
      rank: async id => ctx.$ref.spined.redis.zrank(`${binding.id}.${h}`, id),
      ...$index
    })
  }

  throw new Error(`[spined] unrecognized index canonic ${canonic}`)
}

export default Index
